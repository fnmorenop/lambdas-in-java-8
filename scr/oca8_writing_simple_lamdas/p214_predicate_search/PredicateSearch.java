package oca8_writing_simple_lamdas.p214_predicate_search;

/**
 * Created by fabio.moreno.dlbi on 18/09/2017.
 */

import oca8_writing_simple_lamdas.p209_Animal.Animal;

import java.util.*;
import java.util.function.*;

public class PredicateSearch {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<Animal>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        System.out.printf("%nAnimals that can Hop%n");
        print(animals, a -> a.canHop());
        System.out.printf("%nAnimals that can swim%n");
        print(animals, a -> a.canSwim());
        System.out.printf("%nAnimals that can Hop and swim%n");
        print(animals, a -> a.canSwim() && a.canHop());
        System.out.printf("%nAnimals that can Hop or swim%n");
        print(animals, a -> a.canSwim() || a.canHop());
        System.out.printf("%nAnimals that can Hop and swim functionally%n");
        print(animals, a -> a.canSwim(), b -> b.canHop());
        System.out.printf("%nEspecies%n");
        printSpecie(animals, (Animal a) -> a.canHop());
        System.out.printf("%nEspecies TRUE%n");
        printSpecie(animals, a -> true);
    }
    private static void print(List<Animal> animals, Predicate<Animal> checker) {
        for (Animal animal : animals) {
            if (checker.test(animal))
                System.out.print(animal + " \n");
        }
        System.out.println();
    }

    private static void print(List<Animal> animals, Predicate<Animal> checker,
                              Predicate<Animal> checker2) {
        for (Animal animal : animals) {
            if (checker.test(animal))
                System.out.print(animal + " \n");
            if(checker.and(checker2).test(animal))
                System.out.printf("%nAND: %s%n", animal.toString());
            if(checker.or(checker2).test(animal))
                System.out.printf("%nOR: %s%n", animal.toString());
        }
        System.out.println();
    }

    private static void printSpecie(List<Animal> animals, Predicate<Animal> especie) {
        for (Animal animal : animals) {
            System.out.printf("%nespecie: %s%n", animal.toString());
        }

    }
}
