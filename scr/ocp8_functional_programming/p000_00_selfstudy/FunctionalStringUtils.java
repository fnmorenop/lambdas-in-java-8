package ocp8_functional_programming.p000_00_selfstudy;

import java.util.stream.Collector;
import java.util.stream.IntStream;

public class FunctionalStringUtils {

    public static void main(String[] args) {

        String theString = "Some Words written For TESTING";
        System.out.printf("%ntheString: %s%nlength: %d%n", theString, theString.length());
        String theReversedOrderedString = getReversedOrderedString(theString);
        System.out.printf("%ntheReversedOrderedString: %s%nlength: %d%n", theReversedOrderedString, theReversedOrderedString.length());
        String theOrderedString = getOrderedString (theString);
        System.out.printf("%ntheOrderedString: %s%nlength: %d%n", theOrderedString, theOrderedString.length());
    }

    private static String getOrderedString(String theString) {
        IntStream intStream = theString.chars();
        Collector<Integer, StringBuilder, StringBuilder> charOrderCollector = getOrderCollector ();
        return intStream.boxed().collect(charOrderCollector).toString();
    }

    private static Collector<Integer, StringBuilder, StringBuilder> getOrderCollector() {
        Collector<Integer, StringBuilder, StringBuilder> orderCollector = Collector.of(
                StringBuilder::new,
                (StringBuilder sb, Integer elem) -> {
                    Character c = (char) elem.intValue();
                    if (sb.length() < 1) sb.append(c);
                    else if (c <= sb.charAt(0)) sb.insert(0, c);
                    else if (c >= sb.charAt(sb.length() - 1)) sb.append(c);
                    else for (int i = 0; i + 1 < sb.length(); i++)
                            if (c >= sb.charAt(i) && c < sb.charAt(i+1)){
                                sb.insert(i+1, c);
                                break;
                            }
                },
                (sbOld, sbNew) -> sbOld.append(sbNew)
        );

        return orderCollector;
    }

    private static String getReversedOrderedString(String theString) {
        IntStream intStream = theString.chars();
        Collector<Integer, StringBuilder, StringBuilder> charOrderCollector = getReversedOrderCollector();
        return intStream.boxed().collect(charOrderCollector).toString();
    }

    public static Collector<Integer,StringBuilder,StringBuilder> getReversedOrderCollector() {

        Collector<Integer, StringBuilder, StringBuilder> orderCollector = Collector.of(
                StringBuilder::new,
                (StringBuilder sb, Integer elem) -> {
                    Character c = (char) elem.intValue();
                    if (sb.length() < 1) sb.append(c);
                    else if (c >= sb.charAt(0)) sb.insert(0, c);
                    else if (c <= sb.charAt(sb.length() - 1)) sb.append(c);
                    else for (int i = 0; i + 1 < sb.length(); i++)
                            if (c <= sb.charAt(i) && c >= sb.charAt(i+1)){
                                sb.insert(i+1, c);
                                break;
                            }
                },
                (sbOld, sbNew) -> sbOld.append(sbNew)
        );

        return orderCollector;
    }
}
