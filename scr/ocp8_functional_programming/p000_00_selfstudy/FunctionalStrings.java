package ocp8_functional_programming.p000_00_selfstudy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class FunctionalStrings {

    public static void main(String[] args) {
        //List<Integer> numList = Arrays.asList(1,2,3,6,5,0,9,4);
        Supplier<List<Integer>> numListSupplier = () -> Arrays.asList(1,2,3,6,5,0,9,4);

        System.out.printf("numListSupplier.get(): %s",
                numListSupplier.get());

        System.out.printf("%ngetLeastNumIndex(numListSupplier.get()): %d",
                getLeastNumIndex(numListSupplier.get()));

        System.out.printf("%ngetOrderedList(numListSupplier.get()): %s%n", getOrderedList(numListSupplier.get()));

        System.out.printf("numListSupplier.get(): %s",
                numListSupplier.get());

        System.out.printf("%ngetBiggerNumIndexFunc(numListSupplier).get(): %d",
                getBiggerNumIndexFunc(numListSupplier).get());

        System.out.printf("%ngetOrderedListFunc(numListSupplier): %s%n",
                getOrderedListFunc(numListSupplier).get());

        System.out.printf("numListSupplier.get(): %s",
                numListSupplier.get());

        System.out.printf("%ngetOrderedListFuncFirstPure(numListSupplier, ArrayList::new).get(): %s%n",
                getOrderedListFuncFirstPure(numListSupplier, ArrayList::new).get());

        System.out.printf("numListSupplier.get(): %s",
                numListSupplier.get());

        /*System.out.printf("%ngetOrderedListFuncPure(numListSupplier, ArrayList::new).get(): %s%n",
                getOrderedListFuncPure(numListSupplier, ArrayList::new).get());
                */
    }

    public static Integer getLeastNumIndex (List<? super Integer> numblist) {
        int index = 0;
        for (int i = 1; i < numblist.size(); i++) {
            if((Integer) numblist.get(index) < (Integer) numblist.get(i)) {
                index = i;
            }
        }
        return index;
    }

    public static Integer getBiggerNumIndex (List<? super Integer> numblist) {
        int index = 0;
        List<Integer> receivedList = new ArrayList<>((List<Integer>) numblist);
        for (int i = 1; i < numblist.size(); i++) {
            if( receivedList.get(i) < receivedList.get(index)) {
                index = i;
            }
        }
        return index;
    }

    public static List<? extends Integer> getOrderedList (List<? super Integer> numList) {

        List<Integer> receivedList = new ArrayList<>((List<Integer>) numList);
        List<Integer> orderedList = new ArrayList<>();

        for (int i = 0; i < numList.size(); i++) {
            int index = getBiggerNumIndex(receivedList);
            orderedList.add(receivedList.remove(index));
        }

        return orderedList;
    }

    public static Supplier<? extends Integer> getBiggerNumIndexFunc
            (Supplier<? super List<Integer>> list) {

        Supplier<List<Integer>> receivedSupplier = () -> (List<Integer>) list.get();
        int index = 0;
        for (int i = 1; i < receivedSupplier.get().size(); i++) {
            if(receivedSupplier.get().get(i) < receivedSupplier.get().get(index)) {
                index = i;
            }
        }
        Integer response = index;
        return () -> response;
    }

    public static Supplier<? extends List<Integer>> getOrderedListFunc (Supplier<? super List<Integer>> supplier) {
        List<Integer> receivedNumbs = new ArrayList<>((List<Integer>) supplier.get());
        List<Integer> orderedNums = new ArrayList<>();
        for(int i = 0, y = receivedNumbs.size(); i < y; i++) {
            Supplier<Integer> index = (Supplier<Integer>) getBiggerNumIndexFunc(() ->  receivedNumbs);
            orderedNums.add(receivedNumbs.remove(index.get().intValue()));
        }
        return () -> orderedNums;
    }

    public static Supplier<? extends List<Integer>> getOrderedListFuncFirstPure(Supplier<? super List<Integer>> supplier, Supplier<? super List<Integer>> supplierNew) {
        List<Integer> receivedNumbs = new ArrayList<>((List<Integer>) supplier.get());
        if (receivedNumbs.size() <= 0) return (Supplier<List<Integer>>) supplierNew;
        List<Integer> orderedNums = new ArrayList<>((List<Integer>) supplierNew.get());
        int index = 0;

        for(int i = 1, y = receivedNumbs.size(); i < y; i++) {
            if(receivedNumbs.get(i) < receivedNumbs.get(index)) {
                index = i;
            }
        }

        orderedNums.add(receivedNumbs.remove(index));
        return getOrderedListFuncFirstPure(() -> receivedNumbs, () -> orderedNums);
    }

    /*
     Intent to no remove the items from the source: failed
    public static Supplier<? extends List<Integer>> getOrderedListFuncPure(Supplier<? super List<Integer>> supplier, Supplier<? super List<Integer>> newSupplier) {

        System.out.printf("%nsupplier.get(): %s%n",
                supplier.get());

        List<Integer> receivedNumbs = new ArrayList<>((List<Integer>) supplier.get());

        System.out.printf("receivedNumbs.size(): %d%n",
                receivedNumbs.size());

        if(receivedNumbs.size() < 2 ) return () -> receivedNumbs;

        List<Integer> orderedNums = new ArrayList<>((List<Integer>) newSupplier.get());
        List<Integer> numbsToSend = new ArrayList<>();

        System.out.printf("%nreceivedNumbs.get(receivedNumbs.size() - 2): %d%n",
                receivedNumbs.get(receivedNumbs.size() - 2));
        System.out.printf("%nreceivedNumbs.get(receivedNumbs.size() - 1): %d%n",
                receivedNumbs.get(receivedNumbs.size() - 1));

        if(receivedNumbs.get(receivedNumbs.size() - 2) < receivedNumbs.get(receivedNumbs.size() - 1)) {
            orderedNums.addAll(Arrays.asList(receivedNumbs.get(receivedNumbs.size() - 2), receivedNumbs.get(receivedNumbs.size() - 1)));

        } else {
            orderedNums.addAll(Arrays.asList(receivedNumbs.get(receivedNumbs.size() - 1), receivedNumbs.get(receivedNumbs.size() - 2)));
        }
        return getOrderedListFuncPure(() -> receivedNumbs, () -> orderedNums);
    }
    */
}
