package ocp8_functional_programming.p000_00_selfstudy;


import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExc {

    public static void main(String[] args) {

        Consumer<Integer> printInteger =
                s -> System.out.printf(":%,d:", s);

        int limit = 10;

        System.out.printf("%nReduce Stream: %d%n",
                getStream(limit).reduce(0, (Acc, el) -> Acc + el)
        );

        Collector<Integer, List<Integer>, List<Integer>> sumCollector = Collector.of(
                () -> Arrays.asList(0),
                (acc, e) -> {
                    //System.out.printf("%nacc:%s, e: %d%n", acc, e);
                    acc.set(0, acc.get(0) + e);
                },
                (accO, accN) -> {
                    //System.out.printf("%naccO: %s, accN: %s%n", accO, accN);
                    accO.addAll(accN);
                    return accO;
                }
        );

        System.out.printf("%nsum Collector Stream: %d%n",
                getStream(limit).collect(sumCollector).get(0)
        );

        System.out.printf("%nfunc sum Collector Stream: %d%n",
                getStream(limit).collect(
                        () -> Arrays.asList(0),
                        (acc, e) -> acc.set(0, acc.get(0) + e),
                        List::addAll
                ).get(0)
        );

        /*
        System.out.printf("%nStream sum Collector Stream: %d%n",
                getStream(limit).
                        collect(
                                () -> Stream.of(0),
                                (Stream<Integer> stream, Integer element) ->
                                        Stream.concat(stream, Stream.of(element)),
                                (Stream<Integer> sOld, Stream<Integer> sNew) -> Stream.concat(sOld, sNew)
                        ).
                        peek(printInteger).count()
        );
*/

        System.out.printf("%nFirst Serie%n");
        String fibonacci = Stream.
                iterate(0L, n -> n + 1L).
                //peek(printInteger).
                limit(limit*2).
                //peek(printInteger).
                collect(
                        () -> new ArrayList<Long>(),
                        (l, e) -> {
                            if (l.isEmpty() || l.size() == 1) l.add(e);
                            else l.add(l.get(l.size() - 1) + l.get(l.size() - 2));
                        },
                        List::addAll
                ).toString();
        System.out.printf("%nFibonacci: %s%n", fibonacci);

        System.out.printf("%nFibonacci Serie iterate%n");
        Consumer<Double> printDouble = d -> System.out.printf(":%,.0f:", d);
        Stream.
                iterate(
                        new ArrayList<Double>(),
                        l -> {
                            if(l.isEmpty()) l.add(0D);
                            else if (l.size() == 1) l.add(1D);
                            else l.add(l.get(l.size() - 1) + l.get(l.size() - 2));
                            return l;
                        }
                ).
                limit(limit * 2 + 1).
                filter(l -> l.size() >= (limit * 2) ).
                flatMap(l -> l.stream()).
                forEach(printDouble);

        int[] s = new int[2];

        s[0] = 2;
        s[1] = 4;

        System.out.printf("%ns: %s%n", s);

        int n = 4;
        int[] ar = {3, 2, 1, 3};

        int candles = birthdayCakeCandles(n, ar);
        System.out.printf("%ncandles: %d%n", candles);

        String hourString = "07:05:45PM";
        String formateString = timeConversion(hourString);
        System.out.printf("%nformateString: %s%n", formateString);

    }

    private static Stream<Integer> getStream(Integer limit) {
        return Stream.iterate(1, n -> n + 1).limit(limit);
    }

    static String timeConversion(String s) {
        // Complete this function
        SimpleDateFormat fIn = new SimpleDateFormat("hh:mm:ssa");
        SimpleDateFormat fOut = new SimpleDateFormat("HH:mm:ss");
        Date date;
        try {
            date = fIn.parse(s);
            return fOut.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    static int birthdayCakeCandles(int n, int[] ar) {
        // Complete this function
        int maxHeight = Arrays.
                stream(ar).
                parallel().
                max().
                getAsInt();
        System.out.printf("%nmaxHeight: %d%n", maxHeight);

        return (int) Arrays.
                stream(ar).
                parallel().
                filter(e -> e == maxHeight).
                count();
    }
}
