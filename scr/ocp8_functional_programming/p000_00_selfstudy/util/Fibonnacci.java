package ocp8_functional_programming.p000_00_selfstudy.util;

import java.util.function.DoubleConsumer;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class Fibonnacci {

    private static final double  seed = (1D + Math.sqrt(5)) / 2D;

    public static void main(String[] args) {

        double start = 17D;
        int steps = 5;
        DoubleConsumer printer =
                d -> System.out.printf(": %#4.4f ", d);

        System.out.printf("%nFibonacci Series Decreasing%n");
        getDecrement(start, steps).
                forEach(printer);

        System.out.printf("%nFibonacci Series Increasing%n");
        getIncrement(start, steps).
                forEach(printer);

        System.out.printf("%nFibonacci Series Rounded%n");
        getRounded(start, steps).
                forEach(printer);

        System.out.printf("%nFibonacci Series Rounded - Mapped integer%n");
        getRounded(start, steps).
                mapToInt(d -> (int) d).
                forEach(i -> System.out.printf(": %,d", i));
    }

    public static DoubleStream getDecrement (double start, int steps) {
        return DoubleStream.
                iterate(start, e -> e / seed).
                limit(steps);
    }

    public static DoubleStream getIncrement (double start, int steps) {
        return DoubleStream.
                iterate(start, e -> e * seed).
                limit(steps);
    }

    public static DoubleStream getRounded (double start, int steps) {
        return DoubleStream.
                concat(
                        getDecrement(start, steps).
                                sorted(),
                        getIncrement(start, steps).
                                sorted().
                                filter(e -> e != start)
                );
    }
}
