package ocp8_functional_programming.p000_00_selfstudy.util;

import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.ToLongBiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class FunctionalComparator {



    public static void main(String[] args) throws InterruptedException {

        int limit = 10;

        Printer.title.accept("GetDifference Short Managed String");
        Printer.consumer.accept(
                getDifference().applyAsLong("aaAaaA A", "xxAaaA x")
        );

        Printer.title.accept("GetAltered Animals Stream");
        Printer.consumer.accept(
                getAlterated().apply("aaaaaaaaaa")
        );

        Printer.title.accept("Get lists");
        List<String>  animals = Streams.
                getAnimals().
                get().
                limit(limit).
                collect(Collectors.toList());
        /*List<String> animalsAlterated = Streams.
                getAnimals().
                get().
                map(getAlterated()).
                collect(Collectors.toList());
                */
        List<String> animalsAlterated = animals.
                stream().
                map(getAlterated()).
                collect(Collectors.toList());

        Printer.title.accept("Animals");
        Printer.consumer.accept(animals);

        Printer.title.accept("Altered animals");
        Printer.consumer.accept(animalsAlterated);

        Printer.title.accept("Animals iterated over AlteredAnimals");
        Iterator<String> alteredIterator = animalsAlterated.iterator();
        animals.
                stream().
                mapToLong(s ->
                        getDifference().
                                applyAsLong(s, alteredIterator.next())
                ).
                forEach(Printer.consumer::accept);

        Printer.title.accept("Random words");

        LongSupplier nanoDaySupplier = () -> LocalTime.now().toNanoOfDay();

        int limitWords = 1_00_000_000;
        int maxWordSize = 20;

        // Printer.title.accept("randomWords");

        long start = nanoDaySupplier.getAsLong();

        /*
        List<String> randomWords = Streams.
                getRandomWords(limitWords, maxWordSize).
                get().
                //sequential().
                //peek(Printer.consumer::accept).
                collect(Collectors.toCollection(() -> new ArrayList<>(limitWords)));

        long ranWordsLoad = nanoDaySupplier.getAsLong();
*/
        //Printer.consumer.accept(randomWords);
        //Printer.title.accept("iteratorAlteredRandomWords");
/*
        Iterator<String> iteratorAlteredRandomWords =
                randomWords.
                        stream().
                        map(getAlterated()).
                        //peek(Printer.consumer::accept).
                        collect(Collectors.toCollection(() -> new ArrayList<>(limitWords))).
                        iterator();

        long altRanWordLoad = nanoDaySupplier.getAsLong();
*/
        //Printer.consumer.accept(iteratorAlteredRandomWords);
        //Printer.title.accept("Procc words");
/*
        randomWords.
                stream().
                mapToLong(
                        s -> getDifference().
                                applyAsLong(s, iteratorAlteredRandomWords.next())
                ).
                //peek(Printer.consumer::accept).
                count();

        System.out.printf("%n%nranWordsLoad duration: %,d nanoseconds%n", (ranWordsLoad - start));
        System.out.printf("altRanWordLoad duration: %,d nanoseconds%n", (altRanWordLoad - ranWordsLoad));
        System.out.printf("comparingProc duration: %,d nanoseconds%n", (end - altRanWordLoad));
*/


        limitWords = 8_001_000;//_000;
        Streams.
                getRandomWords(limitWords, maxWordSize).
                get().//sequential(). // Multiply per 10 the processing
                mapToLong(
                        s -> getDifference().
                                applyAsLong(s, getAlterated().apply(s))
                ).
                count();

        long end = nanoDaySupplier.getAsLong();

        System.out.printf("Total Call duration Functional: %,d nanoseconds%n%n", (end - start));
/*
        start = nanoDaySupplier.getAsLong();

        ExecutorService pool = Executors.newCachedThreadPool();

        pool.invokeAll(Streams.
                getRandomWords(limitWords, maxWordSize).
                get().
                map(
                        s -> (Callable<Long>)
                                () ->  getDifference().
                                        applyAsLong(s, getAlterated().apply(s))
                ).collect(Collectors.toList())
        );
        pool.shutdown();
        end = nanoDaySupplier.getAsLong();

        System.out.printf("Total Call duration invokeAll: %,d nanoseconds%n%n", (end - start));

        start = nanoDaySupplier.getAsLong();

        ExecutorService pool2 = Executors.newCachedThreadPool();

        Streams.
                getRandomWords(limitWords, maxWordSize).
                get().
                map(
                        s -> pool2.
                                submit(() -> getDifference().
                                        applyAsLong(s, getAlterated().apply(s))
                        )
                ).
                forEach(
                        f -> new Thread(
                                () -> {
                                    try {
                                        f.get();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } catch (ExecutionException e) {
                                        e.printStackTrace();
                                    } finally {
                                        f.cancel(false);
                                    }
                                }
                                ).start()
                );
                //count();

        new Thread(() -> {int i = 10;}).start();

        pool2.shutdown();
        end = nanoDaySupplier.getAsLong();

        System.out.printf("Total Call duration Streaming future: %,d nanoseconds%n%n", (end - start));
*/
    }

    public static ToLongBiFunction<String, String> getDifference() {

        return (s1, s2) ->
                Optional.
                        of(s1).
                        filter(s -> s.length() == s2.length()).
                        map(
                                s -> {
                                    Iterator<Integer> cs2 = s2.chars().iterator();
                                    return s.chars().
                                            filter(i -> i != cs2.next()).
                                            count();
                                }
                        ).orElse(-1L);
    }

    public static Function<String, String> getAlterated () {

        return s -> {

            double rand = Math.random();
            StringBuilder sb = new StringBuilder();
            if (rand <= 0.33D)
                sb.append(s);
            else if (rand > 0.33D && rand <= 0.66D)
                sb.append("--xx--");
            else
                sb.append(s.
                        chars().
                        mapToObj(
                                i -> {
                                    double randM = Math.random();
                                    char response = (char) i;
                                    if(randM < 0.33D)
                                        response +=  5;
                                    return response;
                                }
                        ).
                        collect(
                                () -> new StringBuilder(),
                                StringBuilder::append,
                                StringBuilder::append
                        )
                );
            return  sb.toString();
        };
    }
}
