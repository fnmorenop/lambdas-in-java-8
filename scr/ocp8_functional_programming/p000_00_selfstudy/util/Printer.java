package ocp8_functional_programming.p000_00_selfstudy.util;

import java.util.*;
import java.util.function.*;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Printer {
    public static final DoubleConsumer doubleConsumer = getDoubleConsumer ();
    public static final IntConsumer intConsumer = geIntConsumer ();
    public static final LongConsumer longConsumer = getLongConsumer ();
    public static final Consumer<String> stringConsumer = getStringConsumer ();
    public static final Consumer<String> title = getTitleConsumer ();
    public static final Consumer consumer = getConsumer ();
    public static final Consumer<Optional> optionalConsumer = getOptionalConsumer ();


    public static DoubleConsumer getDoubleConsumer () {

        return d -> System.out.printf(": %.1f ", d);
    }

    public static  IntConsumer geIntConsumer () {

        return i -> System.out.printf(": %,d ", i);
    }

    public static LongConsumer getLongConsumer () {

        return l -> System.out.printf(": %,d ", l);
    }

    public static Consumer<String> getStringConsumer () {

        return s -> System.out.printf(": %s ", s);
    }

    public static Consumer<String> getTitleConsumer () {

        return s -> System.out.printf("%n%n%s%n%n", s);
    }

    public static Consumer getConsumer () {

        return  eo -> {

            if ( eo instanceof Double )
                doubleConsumer.accept((Double) eo);
            else if ( eo instanceof Integer)
                intConsumer.accept((Integer) eo);
            else if (eo instanceof Long)
                longConsumer.accept((Long) eo);
            else if (eo instanceof String)
                stringConsumer.accept((String) eo);
            else if (eo instanceof Optional)
                optionalConsumer.accept((Optional) eo);
            else if (eo instanceof Stream)
                ((Stream) eo).forEach(consumer);
            else if (eo instanceof DoubleStream)
                ((DoubleStream) eo).forEach(doubleConsumer);
            else if (eo instanceof IntStream)
                ((IntStream) eo).forEach(intConsumer);
            else if (eo instanceof LongStream)
                ((LongStream) eo).forEach(longConsumer);
            else if (eo instanceof Supplier)
                consumer.accept(((Supplier) eo).get());
            else if (eo instanceof Iterator)
                ((Iterator) eo).forEachRemaining(consumer);
            else if (eo instanceof Iterable)
                ((Iterable) eo).forEach(consumer);
            else if (eo instanceof Spliterator)
                ((Spliterator) eo).tryAdvance(consumer);
            else
                consumer.accept(eo.toString());
        };
    }

    public static Consumer<Optional> getOptionalConsumer () {

        return o -> o.ifPresent(consumer);
    }
}
