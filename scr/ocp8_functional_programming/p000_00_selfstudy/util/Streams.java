package ocp8_functional_programming.p000_00_selfstudy.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {

    public static String root = "C:\\learn\\lambdas-in-java-8\\scr";
    public static String packageName = "ocp8_functional_programming\\p000_00_selfstudy\\sources";
    public static String animalFileName = "animals.txt";

    public static void main(String[] args) {

        Printer.title.accept("getAnimals");
        getAnimals().
                get().
                forEach(Printer.consumer);

        Printer.title.accept("Print word");
        getWord(100).
                get().
                forEach(Printer.consumer);

        Printer.title.accept("Print Words");
        getRandomWords(100, 60).
                get().
                forEach(Printer.consumer);

        Integer a = (int) 'a';
        Integer b = (int) 'b';


        System.out.printf("%n%na: %s%n%n", Integer.toBinaryString(a));
        System.out.printf("%n%nb: %s%n%n", Integer.toBinaryString(b));
        System.out.printf("%n%na ^ b: %s%n%n", Integer.toBinaryString(a ^ b));
        System.out.printf("%n%na ^ b: %d%n%n", a ^ b);

        System.out.printf("%d", Stream.concat(Stream.concat(Stream.of(10), Stream.empty()), Stream.of(20)).count());

        Printer.title.accept("Stream duplicate numbers");
        getDuplicatedNumbers(10).get().forEach(Printer.consumer::accept);

    }

    public static Supplier<Stream<String>> getFileStream (String root, String packageName, String fileName) {

        Path path = Paths.get(root, packageName, fileName);
        return () -> {
            try {
                return Files.readAllLines(path).stream();
            } catch (IOException e) {
                return Stream.of(e.toString());
            }
        };
    }

    public static Supplier<Stream<String>> getAnimals () {

        return getFileStream(root, packageName, animalFileName);
    }

    public static Supplier<Stream<String>> getRandomWords (long limit, long maxWordSize) {

        return () -> new Random().
                ints(limit, 1, (int) maxWordSize).
                parallel().
                mapToObj( i -> getWord(i).
                        get().
                        reduce(String::concat).
                        get()
                );
    }

    public static Supplier<Stream<String>> getWord (long maxWordSize) {
        return () ->
            Stream.of(
                    /*IntStream.
                            generate( () -> (int) (Math.random() * (double) 'z' + 1)).
                            filter(c -> Character.isAlphabetic(c) || Character.isSpaceChar(c)).*/
                    new Random().
                            ints(maxWordSize, ' ', 'z' + 1).
                            parallel().
                            filter(c -> Character.isAlphabetic(c) || Character.isSpaceChar(c)).
                            /*mapToObj(
                                    i -> (char) i
                            ).*/
                            collect(
                                    () -> new StringBuilder(),
                                    (st, c) -> st.append(Character.toString((char) c)),
                                    StringBuilder::append
                            ).toString()
                    );
        }

    public static Supplier<IntStream> getDuplicatedNumbers (int maxBound) {
        return () -> new Random().
                        ints(maxBound, 0, maxBound + 1).
                        //rangeClosed(0, maxBound).
                        flatMap(
                                //i -> IntStream.concat(IntStream.of(i), IntStream.of(i))
                                i -> IntStream.of(i, i)
                        );
    }

}
