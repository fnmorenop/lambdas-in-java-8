package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

public class Animal {
    private String species;
    private boolean canHop;
    private boolean canSwim;

    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() { return canHop; }

    public boolean canSwim() { return canSwim; }

    public String toString() { return species; }
}
