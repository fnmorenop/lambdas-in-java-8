package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

// Non functional interface, defines two abstract mehthods
// @FunctionalInterface
public interface Crawl {
    public void crawl ();
    public int getCount ();
}
