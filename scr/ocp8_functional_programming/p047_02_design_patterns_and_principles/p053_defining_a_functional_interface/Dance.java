package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

// Non functional interface, extends sprint abstract method and joins another method
public interface Dance extends Sprint {
    public void dance (Animal animal);
}
