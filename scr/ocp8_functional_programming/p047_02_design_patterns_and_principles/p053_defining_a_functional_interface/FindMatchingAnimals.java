package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

public class FindMatchingAnimals {

    private static void print (Animal animal, CheckTrait trait) {
        if (trait.test(animal))
            System.out.println(animal);
    }

    private static void name (Animal animal, NameTrait nameTrait) {
        System.out.printf("%nThe animal's name: %s%n", nameTrait.getName(animal));
    }

    private static void printAnimal (Animal animal, AnimalTrait animalTrait) {
        System.out.println("The animal structure is: \n");
        System.out.printf("%nEspecia: %s,%nHopper: %b,%nSwimmer: %b",
                animal.toString(), animal.canHop(), animal.canSwim());
    }

    private static void printNoParameter(Animal animal, NoParameterTrait noParameterTrait) {
        System.out.println("The printNoParameter struscture is: \n");
        System.out.printf("The called structure with no parameters is: %s", noParameterTrait.getMethodWithNoParameter());
    }

    public static void main(String[] args) {
        System.out.println("-- The TraitCheck interface --");
        print(new Animal("fish", false, true), a -> a.canHop());
        print(new Animal("kangaroo", true, false), a -> a.canHop());
        print(new Animal("kangarrrooo", true, true), a -> a.canSwim());

        System.out.println("-- The NameTrait interface --");
        name(new Animal("fish", false, true), a -> a.toString());
        name(new Animal("Kangarooo", false, true), a -> "HardCoded String");
        name(new Animal("Rabbit", true, false), a -> a.toString());

        System.out.println("-- The AnimalTrait interface --");
        name(new Animal("fish", false, true), a -> a.toString());

        System.out.println("-- PrintAnimal Method --");
        printAnimal(new Animal("PrintAnimalfish", false, true), a -> a);

        System.out.println("-- The NoParameterTrait interface --");
        printNoParameter(new Animal("fishWithNoParameter", true, false), () -> "The name of the Animal is uncertain");
    }
}
