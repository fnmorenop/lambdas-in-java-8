package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

@FunctionalInterface
public interface Skip extends Sprint {

    public default int getHopCount (Animal kangaroo) {
        return 10;
    }

    public static void skip (int speed) {}
}
