package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

@FunctionalInterface
public interface Sprint {
    public void sprint(Animal animal);

    // public void nonFunctionalMethod (Animal animal);
}
