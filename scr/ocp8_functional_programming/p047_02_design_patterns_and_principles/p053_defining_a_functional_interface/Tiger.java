package ocp8_functional_programming.p047_02_design_patterns_and_principles.p053_defining_a_functional_interface;

public class Tiger implements Sprint {
    public void sprint (Animal animal) {
        System.out.println("Animal is sprinting fast? " + animal.toString());
    }

//    public void nonFunctionalMethod (Animal animal) {
//        System.out.println("Animal is sprinting fast? " + animal.toString());
//    }
}
