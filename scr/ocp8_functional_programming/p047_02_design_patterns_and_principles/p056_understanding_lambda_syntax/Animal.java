package ocp8_functional_programming.p047_02_design_patterns_and_principles.p056_understanding_lambda_syntax;

public class Animal {

    String name = "The parent Animal";

    public Animal () {}

    public Animal (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    @Override
    public String toString() {
        return String.format("The animal toString: %s%n", name);
    }
}
