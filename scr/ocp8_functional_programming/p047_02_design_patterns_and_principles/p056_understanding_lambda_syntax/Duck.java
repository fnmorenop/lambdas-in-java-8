package ocp8_functional_programming.p047_02_design_patterns_and_principles.p056_understanding_lambda_syntax;

public class Duck extends Animal{
    String name = "Default DUCK Name";

    public Duck () {}

    public Duck (String name) {
        this.name = name;
    }

    public String quack() {
        return "Quack, quack, quack";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isMakeQuacke () {
        return true;
    }

    @Override
    public String toString() {
        return String.format("Quack name: %s, IsMakeQuacke: %s", getName(), isMakeQuacke());
    }
}
