package ocp8_functional_programming.p047_02_design_patterns_and_principles.p056_understanding_lambda_syntax;

public class P057_ValidLambdaExpressions {

    public static void main(String[] args) {
        String elDuckName = "El Duck";
        String elNoDuckName = "El NO Duck";
        String elAnimalName = "El Animal";

        Duck elDuck = new Duck(elDuckName);
        Duck elNoDuck = new Duck (elNoDuckName);
        Animal elAnimal = new Animal(elAnimalName);

        printWithNoParameters (elNoDuck, () -> elNoDuck);

        System.out.printf("%n%s%n", elDuck);
        System.out.printf("%n%s%n", elNoDuck);

        makeQuake(elDuck, d -> {return d.isMakeQuacke();} );

        makeQuake(elNoDuck, (Duck duck) -> duck.isMakeQuacke());

        isAnimal(new Animal(), elDuck, (Animal a, Duck d) -> d instanceof Animal && a instanceof Animal);

        isAnimal(elDuck, elNoDuck, (Animal a, Duck d) -> true);

        isAnimal(elDuck, elNoDuck, (a, d) -> false);

        // isName(elDuck, elDuckName, (Animal a, String theName) -> a.getName().equals(theName));

        isName(elDuck, elDuckName, (Animal a, String theName) -> {
            System.out.printf("%n--This is inside the isName lambda expresion for a trait%n");
            Boolean isEqual = a.getName().equals(theName);
            return isEqual;
        });

        showAnimal(elAnimal, a -> {});

        showAnimal(elDuck, a -> {return;});
    }

    public static void printWithNoParameters (Duck duck, noParameterTrait noParameterTrait) {
        if (noParameterTrait.resolve() instanceof Duck) {
            System.out.printf ("%nThe Duck is a correct instance of Duck%n");
            System.out.printf ("%nThe default Duck name is: %s%n", duck.getName());
            changeDuckName(duck, () -> "Changed DuckName");
            System.out.printf ("%nThe default Duck name was changed to: %s%n", duck.getName());
        } else {
            System.out.printf ("%nThe Duck wasn't caught as a good instance%n");
        }
    }

    public static String changeDuckName (Duck duck, ChangeDuckNameTrait changeDuckNameTrait) {
        duck.setName(changeDuckNameTrait.changeName());
        return duck.getName();
    }

    public static void makeQuake (Duck duck, DuckQuackTrait duckQuackTrait) {
        if (duckQuackTrait.makeQuack(duck)) {
            System.out.printf("%nThe duck %s, says: %s%n", duck.getName(), duck.quack());
        } else {
            System.out.printf("%nThe duck doesn't appear to be Quack%n");
        }
    }

    public static void isAnimal (Animal animal, Duck duck, TwoParameterTrait twoParameterTrait) {
        if (twoParameterTrait.isParent (animal, duck)) {
            System.out.printf("%nThe animal: %s, and the duck: %s, are Animal%n", animal.getName(), duck.getName());
        } else {
            System.out.printf("%nThe duck doesn't appear to be a duck%n");
        }
    }


    public static void isName (Animal animal, String name, TwoDifferentParameterTrait twoDifferentParameterTrait) {
    // public static void isName (Animal animal, String name, ParentTrait parentTrait) {
        // TwoDifferentParameterTrait twoDifferentParameterTrait = (TwoDifferentParameterTrait) parentTrait; // Doesn't Compile
         if (twoDifferentParameterTrait.isName(animal, name)) {
        //if (parentTrait.isName(animal, name)) {
            System.out.printf("%nThe duck name's %s%n", name);
        } else {
            System.out.printf("%nThe duck name's different, is: %s%n", animal.getName());
        }
    }

    public static void showAnimal (Animal animal, NoReturnTrait noReturnTrait) {
        System.out.printf("%n--Inside showAnimal method%n");
        // System.out.printf("%nThe animal traits are: %s%n", animal.toString());
        // noReturnTrait.showAnimal(animal); // It appears to be non sense
        System.out.printf("%nThe animal traits are: %s%n", animal);
    }
}


interface ParentTrait {
    Boolean isName (Animal animal, String name);
}

@FunctionalInterface
interface noParameterTrait {
    Duck resolve();
}

//@FunctionalInterface
interface ChangeDuckNameTrait {
    String changeName ();
}

interface DuckQuackTrait {
    Boolean makeQuack (Duck duck);
}

interface TwoParameterTrait{
    Boolean isParent (Animal animal, Duck duck);
}

@FunctionalInterface
interface TwoDifferentParameterTrait extends ParentTrait {
    @Override
    Boolean isName (Animal animal, String name);
}

interface NoReturnTrait {
    void showAnimal (Animal animal);
}