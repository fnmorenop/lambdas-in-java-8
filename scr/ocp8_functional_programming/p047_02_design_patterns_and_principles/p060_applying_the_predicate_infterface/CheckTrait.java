package ocp8_functional_programming.p047_02_design_patterns_and_principles.p060_applying_the_predicate_infterface;

public interface CheckTrait<Animal> {
    public Boolean test (Animal animal);
}
