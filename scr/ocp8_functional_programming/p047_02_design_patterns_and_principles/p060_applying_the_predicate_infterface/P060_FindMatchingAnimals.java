package ocp8_functional_programming.p047_02_design_patterns_and_principles.p060_applying_the_predicate_infterface;

import java.util.function.Predicate;

public class P060_FindMatchingAnimals {

    private static void print (Animal animal, Predicate<Animal> trait) {
        if (trait.test(animal)) {
            System.out.printf("%nThe animal: %s", animal);
        }
    }

    public static void main(String[] args) {
        print (new Animal("fish", false, true), animal -> animal.canHop());
        print (new Animal("Kangaroo", true, false), animal -> animal.canHop());
    }
}
