package ocp8_functional_programming.p047_02_design_patterns_and_principles.p061_implementing_polymorphism;

public class Dolphin implements LivesInOcean{

    @Override
    public void makeSound() {
        System.out.println("Whistle");
    }
}
