package ocp8_functional_programming.p047_02_design_patterns_and_principles.p061_implementing_polymorphism;

public class P062_Lemur extends Primate implements HasTail {

    public int age = 10;

    @Override
    public boolean isTailStriped() {
        return false;
    }

    public static void main(String[] args) {
        P062_Lemur lemur = new P062_Lemur();
        System.out.printf("%nThe lemur's age is: %d%n", lemur.age);

        HasTail hasTail = lemur;
        System.out.printf("%nIs the lemmur's tail striped: %b%n", hasTail.isTailStriped());

        Primate primate = lemur;
        System.out.printf("%nHas the lemmur hair: %b%n", primate.hasHair());

        P062_Lemur lemur2 = lemur;
        System.out.printf("%nThe lemur's age is: %d%n", lemur2.age);
    }
}
