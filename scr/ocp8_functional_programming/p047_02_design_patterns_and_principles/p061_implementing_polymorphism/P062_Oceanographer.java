package ocp8_functional_programming.p047_02_design_patterns_and_principles.p061_implementing_polymorphism;

public class P062_Oceanographer {

    public void checkSound (LivesInOcean animal) {
        animal.makeSound();
    }

    public static void main(String[] args) {
        P062_Oceanographer oceanographer = new P062_Oceanographer();
        oceanographer.checkSound(new Dolphin());
        oceanographer.checkSound(new Whale());
    }
}
