package ocp8_functional_programming.p047_02_design_patterns_and_principles.p061_implementing_polymorphism;

public class Primate {

    public boolean hasHair () {
        return true;
    }
}
