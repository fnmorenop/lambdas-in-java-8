package ocp8_functional_programming.p103_03_generics_and_collections.p152_using_method_references;

public class Duck {

    private String name;
    private int weight;

    public Duck(String name, int weight) {
        this.setName(name);
        this.setWeight(weight);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return String.format("{Name: %s, weight: %s}", name, weight);
    }
}
