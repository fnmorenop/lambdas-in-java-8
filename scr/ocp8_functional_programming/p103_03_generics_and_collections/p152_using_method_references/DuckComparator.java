package ocp8_functional_programming.p103_03_generics_and_collections.p152_using_method_references;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DuckComparator {

    static Comparator<Duck> byWeightTraditional = new Comparator<Duck>() {
        @Override
        public int compare(Duck d1, Duck d2) {
            return DuckHelper.compareByWeight(d1, d2);
        }
    };

    static Comparator<Duck> byNameTradicional = new Comparator<Duck>() {
        @Override
        public int compare(Duck d1, Duck d2) {
            return DuckHelper.compareByName(d1, d2);
        }
    };

    static Comparator<Duck> byWeightFunctional = (d1, d2) -> DuckHelper.compareByWeight(d1, d2);
    static Comparator<Duck> byNameFunctional = (d1, d2) -> DuckHelper.compareByName(d1, d2);

    static Comparator<Duck> byWeightMethodReference = DuckHelper::compareByWeight;
    static Comparator<Duck> byNameMethodReference = DuckHelper::compareByName;

    public static void main(String[] args) {

        List<Duck> ducks = getDuckList();

        System.out.printf("%nThe just created duck list is:%n%s%n", ducks);

        Collections.sort(ducks, byNameMethodReference);

        System.out.printf("%nThe sorted duck list duck list is:%n%s%n", ducks);
/*
        ducks = getDuckList();

        System.out.printf("%nThe just created duck list is:%n%s%n", ducks);

        Collections.sort(ducks, byWeightMethodReference);

        System.out.printf("%nThe sorted duck list duck list is:%n%s%n", ducks);
*/

        ducks = getDuckList();
    }

    public static List<Duck> getDuckList () {

        List<Duck> ducks = new ArrayList<>();

        ducks.add(new Duck("El duck 1", 1));
        ducks.add(new Duck("El duck 3", 3));
        ducks.add(new Duck("El duck 5", 5));
        ducks.add(new Duck("El duck 2", 2));
        ducks.add(new Duck("El duck 4", 4));
        ducks.add(new Duck("El duck 6", 6));

        return ducks;
    }
}
