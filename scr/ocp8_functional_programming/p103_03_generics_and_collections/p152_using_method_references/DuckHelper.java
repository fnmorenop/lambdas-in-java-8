package ocp8_functional_programming.p103_03_generics_and_collections.p152_using_method_references;

public class DuckHelper {

    public static int compareByWeight (Duck d1, Duck d2) {
        return d1.getWeight() - d2.getWeight();
    }

    public static int compareByName (Duck d1, Duck d2) {
        return d1.getName().compareTo(d2.getName());
    }

}
