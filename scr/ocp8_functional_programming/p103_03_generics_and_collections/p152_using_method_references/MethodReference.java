package ocp8_functional_programming.p103_03_generics_and_collections.p152_using_method_references;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MethodReference {

    public static void main(String[] args) {

        Consumer<List<Integer>> methodRef1 = Collections::sort;
        Consumer<List<Integer>> lambda1 = l -> Collections.sort(l);

        List numbList = getNumbersList();

        System.out.printf("%n Consumer<List<Integer>> %n");
        System.out.printf("numbList: %s%n", numbList);

        methodRef1.accept(numbList);
        System.out.printf("Arranged numbList: %s%n", numbList);

        String str = "abc";
        Predicate<String> methodRef2 = str::startsWith;
        Predicate<String> lambda2 = s -> str.startsWith(s);

        System.out.printf("%nPredicate<String>%n");
        System.out.printf("methodRef2.test(\"a\"): %b%n", methodRef2.test("a"));
        System.out.printf("methodRef2.test(\"x\"): %b%n", methodRef2.test("x"));

        Predicate<String> methodRef3 = String::isEmpty;
        Predicate<String> lambda3 = s -> s.isEmpty();

        String emptyString = "";
        String nonEmptyString = "Non Empty";

        System.out.printf("methodRef3.test(emptyString): %b%n", methodRef3.test(emptyString));
        System.out.printf("methodRef3.test(nonEmptyString): %b%n", methodRef3.test(nonEmptyString));

        System.out.printf("%nSuplier<ArrayList>%n");

        Supplier<ArrayList> methodRef4 = ArrayList::new;
        Supplier<ArrayList> lambda4 = () -> new ArrayList();

        System.out.printf("numbList: %s%n", numbList);
        numbList = methodRef4.get();
        System.out.printf("new numbList: %s%n", numbList);

        Supplier<List<Integer>> supNumb = Arrays::asList;
        Supplier<List<Integer>> supNumbF = () -> Arrays.asList(1,2,3,4,5,6,0,9,8,7);

        numbList = supNumbF.get();
        System.out.printf("new numbList: %s%n", numbList);

        methodRef1.accept(numbList);
        System.out.printf("Arranged numbList: %s%n", numbList);

        numbList = supNumb.get();
        System.out.printf("new numbList: %s%n", numbList);

        System.out.printf("%nprintList(getList())%n");

        Supplier<List<Integer>> intNumbList = getList();

        System.out.printf("%n printList(intNumbList)%n");

        printList(intNumbList);

        intNumbList = getEmptyList();

        System.out.printf("%n printList(intNumbList)%n");

        printList(intNumbList);

        System.out.printf("%nprintList(() -> Arrays.asList(1,2,3,4,5,5,4,3,2,1))%n");

        printList(() -> Arrays.asList(1,2,3,4,5,5,4,3,2,1));

        System.out.printf("%nintNumbList = MethodReference::getNumbersList%n");

        intNumbList = MethodReference::getNumbersList;

        System.out.printf("%nprintList(intNumbList)%n");

        printList(intNumbList);

        System.out.printf("%nConsumer<List<Integer>> contraSort = list -> Collections.sort(list,\n" +
                "                (o1, o2) -> o1 == o2 ? 0 : o1 > o2 ? -1 : 1)%n");

        Consumer<List<Integer>> contraSort = list -> Collections.sort(list,
                (o1, o2) -> o1 == o2 ? 0 : o1 > o2 ? -1 : 1);

        System.out.printf("%nnumbList = intNumbList.get()%n");

        numbList = intNumbList.get();

        System.out.printf("numbList: %s%n", numbList);

        System.out.printf("%ncontraSort.accept(numbList)%n");

        contraSort.accept(numbList);

        System.out.printf("numbList: %s%n", numbList);

        System.out.printf("%nComparator<Integer> sort = (o1, o2) -> o1 == o2 ? 0 : o1 > o2 ? 1 : -1%n");

        Comparator<Integer> sort = (o1, o2) -> o1 == o2 ? 0 : o1 > o2 ? 1 : -1;

        System.out.printf("%ncontraSort = list -> Collections.sort(list, sort);%n");

        System.out.printf("numbList: %s%n", numbList);

        contraSort = list -> Collections.sort(list, sort);

        System.out.printf("numbList: %s%n", numbList);

        contraSort.accept(numbList);

        System.out.printf("numbList: %s%n", numbList);
    }

    private static List<Integer> getNumbersList () {
        return Arrays.asList(0,9,8,7,6,5,4,3,2,1);
    }

    private static Supplier<List<Integer>> getEmptyList () {
        return Arrays::asList;
    }

    private static Supplier<List<Integer>> getList () {
        return () -> Arrays.asList(1,2,3,4);
    }

    private static void printList (Supplier<List<Integer>> list) {
        System.out.printf("%nThe list is: %s%n", list.get());
    }

}
