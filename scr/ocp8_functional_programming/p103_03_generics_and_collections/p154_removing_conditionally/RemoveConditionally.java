package ocp8_functional_programming.p103_03_generics_and_collections.p154_removing_conditionally;

import java.util.ArrayList;
import java.util.List;

public class RemoveConditionally {

    public static void main(String[] args) {

        List<String> list = getDefaultList();
        System.out.printf("%nThe list is formed by:%n%s%n", list);

        list.removeIf(s -> s.startsWith("A") || s.startsWith("W"));
        System.out.printf("%nThe filtered list is as:%n%s%n", list);

        list = getDefaultList();
        System.out.printf("%nThe list is formed by:%n%s%n", list);


        int maxLenght = 5;
        list.replaceAll(it -> it.length() > maxLenght ? it.substring(0, maxLenght) : it);
        System.out.printf("%nThe filtered list is as:%n%s%n", list);

    }

    public static List<String> getDefaultList () {

        List<String> list = new ArrayList<>();

        list.add("Magician");
        list.add("Assistant");
        list.add("Spectator");
        list.add("Watchman");

        return list;
    }
}
