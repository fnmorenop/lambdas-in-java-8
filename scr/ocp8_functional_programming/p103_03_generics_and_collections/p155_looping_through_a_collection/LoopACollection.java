package ocp8_functional_programming.p103_03_generics_and_collections.p155_looping_through_a_collection;

import java.util.Arrays;
import java.util.List;

public class LoopACollection {

    public static void main(String[] args) {

        List<String> cats = Arrays.asList("Annie", "Ripley", "Meg");

        System.out.println("The tradicional forEach for loop:");
        for (String cat : cats) {
            System.out.println(cat);
        }
        System.out.println();

        System.out.println("The functional version of forEach for loop:");
        cats.forEach(s -> System.out.println(s));
        System.out.println();

        System.out.println("The reference method version of forEach for loop:");
        cats.forEach(System.out::println);
        System.out.println();
    }
}
