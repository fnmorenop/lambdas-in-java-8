package ocp8_functional_programming.p103_03_generics_and_collections.p155_updating_all_elements;

import java.util.Arrays;
import java.util.List;

public class UpdateAllElements {

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.replaceAll(x -> x * 2);
        System.out.printf("%nThe updated list is as: %n%s%n", list);
    }
}
