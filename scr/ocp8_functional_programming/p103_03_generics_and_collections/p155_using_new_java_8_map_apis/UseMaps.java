package ocp8_functional_programming.p103_03_generics_and_collections.p155_using_new_java_8_map_apis;

import java.util.HashMap;
import java.util.Map;

public class UseMaps {

    public static void main(String[] args) {

        Map<String, String> favorites;

        {
            favorites = new HashMap<>();
            favorites.put("Jenny", "Bus Tour");
            favorites.put("Jenny", "Tram");

            System.out.printf("%nMap value overriding%n%s%n", favorites);
        }

        {


            favorites = new HashMap<>();

            favorites.put("Jenny", "Bus Tour");
            favorites.put("Tom", null);

            favorites.putIfAbsent("Jenny", "Tram");
            favorites.putIfAbsent("Sam", "Tram");
            favorites.putIfAbsent("Tom", "Tram");

            System.out.printf("%nMap value ifAbsent overriding%n%s%n", favorites);
        }


    }
}
