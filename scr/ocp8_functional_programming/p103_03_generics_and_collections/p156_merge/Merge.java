package ocp8_functional_programming.p103_03_generics_and_collections.p156_merge;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Merge {

    public static void main(String[] args) {
        {
            System.out.printf("%nBiFunction in merge map%n");

            BiFunction<String, String, String> mapper = (v1, v2) -> v1.length() > v2.length() ? v1 : v2;

            Map<String, String> favorites = new HashMap<>();
            favorites.put("Jenny", "Bus Tour");
            favorites.put("Tom", "Tram");

            String jenny = favorites.merge("Jenny", "Skyride", mapper);
            String tom = favorites.merge("Tom", "Skyride", mapper);

            System.out.printf("%nFavorites: %s%n", favorites);
            System.out.printf("%nJenny: %s%n", jenny);
            System.out.printf("%nTom: %s%n", tom);
        }

        {
            System.out.printf("%nBifunction in merge exercices%n");

            BiFunction<String, String, String> mapper =
                    (v1, v2) ->
                    {
                        System.out.printf("(v1, v2): (%6s, %s)%n", v1, v2);
                        if ( v1.isEmpty() )
                            return null;
                        else
                            return String.format( "%s squared is: %.0f", v1,
                                Math.pow(Double.valueOf(v2), 2));
                    };

            Map<String, String> store = (Map<String, String>) getStore();

            System.out.printf("%nstore: %s%n", store);

            System.out.printf("mappedStore: %s%n", mapStore(store, mapper));
        }
    }

    public static Map<? extends String, ? extends String> getStore () {
        Map<String, String> store = new HashMap<>();
        store.put("0", "Zero");
        store.put("1", "One");
        store.put("2", "Two");
        store.put("3", "Three");
        store.put("4", "Four");
        store.put("5", null);
        store.put("6", "");
        return store;
    }

    public static Map <? extends String, ? extends String> mapStore (
            Map <? super String, ? super String> store,
            BiFunction <? super String, ? super String, ? super String> mapper) {

        BiFunction <String, String, String> rMapper =
                (BiFunction<String, String, String>) mapper;
        Map <String, String> rStore = (Map<String, String>) store;

        for (String index : rStore.keySet()) {
            rStore.merge(index, index, rMapper);
        }

        rStore.merge("7", "Seven squared is: 49", rMapper);

        return rStore;
    }
}
