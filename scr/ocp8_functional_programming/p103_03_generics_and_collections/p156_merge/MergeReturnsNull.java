package ocp8_functional_programming.p103_03_generics_and_collections.p156_merge;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class MergeReturnsNull {

    public static void main(String[] args) {

        BiFunction<String, String, String> mapper = (v1, v2) -> null;

        Map<String, String> favorites = new HashMap<>();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", "Bus Tour");

        favorites.merge("Jenny", "Skyrider", mapper);
        favorites.merge("Sam", "Skyrider", mapper);

        System.out.printf("%nFavorites: %s%n", favorites);
    }
}
