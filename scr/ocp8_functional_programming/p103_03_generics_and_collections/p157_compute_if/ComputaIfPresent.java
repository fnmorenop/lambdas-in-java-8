package ocp8_functional_programming.p103_03_generics_and_collections.p157_compute_if;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ComputaIfPresent {

    public static void main(String[] args) {

        Map<String, Integer> counts;

        {
            counts = getCounts();

            // ifPresent
            BiFunction<String, Integer, Integer> mapper = (k, v) -> v + 1;

            Integer jenny = counts.computeIfPresent("Jenny", mapper);
            Integer sam = counts.computeIfPresent("Sam", mapper);
            Integer thom = counts.computeIfPresent("Thom", mapper);
            Integer nullKey = counts.computeIfPresent(null, mapper);

            System.out.printf("getCounts: %12s%n", getCounts());
            System.out.printf("ifPresent Counts: %12s%n", counts);
            System.out.printf("Jenny: %12s%n", jenny);
            System.out.printf("Sam: %12s%n", sam);
            System.out.printf("NullKey: %12s%n", nullKey);
            System.out.printf("Thom: %12s%n", thom);
        }

        {
            counts = getCounts();

            // ifAbsent
            Function<String, Integer> mapper = (k) -> 1;

            Integer jenny = counts.computeIfAbsent("Jenny", mapper);
            Integer sam = counts.computeIfAbsent("Sam", mapper);
            Integer thom = counts.computeIfAbsent("Thom", mapper);
            Integer nullKey = counts.computeIfAbsent(null, mapper);

            System.out.printf("%ngetCounts: %12s%n", getCounts());
            System.out.printf("ifAbsent Counts: %12s%n", counts);
            System.out.printf("Jenny: %12s%n", jenny);
            System.out.printf("Sam: %12s%n", sam);
            System.out.printf("NullKey: %12s%n", nullKey);
            System.out.printf("Thom: %12s%n", thom);
        }

        {
            counts = getCounts();

            counts.computeIfPresent("Jenny", (k, v) -> null );
            counts.computeIfPresent("Sam", (k, v) -> null);
            counts.computeIfPresent("Thom", (k, v) -> null);
            counts.computeIfPresent(null, (k, v) -> null);

            System.out.printf("%ngetCounts: %12s%n", getCounts());
            System.out.printf("Counts: %12s%n", counts);

            counts = getCounts();

            counts.computeIfAbsent("Jenny", k -> null);
            counts.computeIfAbsent("Sam", k -> null);
            counts.computeIfAbsent("Thom", k -> null);
            counts.computeIfAbsent(null, (k) -> null);

            System.out.printf("%ngetCounts: %12s%n", getCounts());
            System.out.printf("Counts: %12s%n", counts);
        }


    }

    public static Map<String, Integer> getCounts () {

        Map<String, Integer> counts = new HashMap<>();

        counts.put("Jenny", 10);
        counts.put("Sam", null);
        counts.put("Tom", null);
        counts.put("Jen", 10);

        return counts;
    }
}
