package ocp8_functional_programming.p103_03_generics_and_collections.p157_compute_if;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class ComputeExcs {

    public static void main(String[] args) {
        Supplier < Map < Integer, String > > supplierMap = getSupplierMap ();

        printMap ( supplierMap );

        BiFunction < Integer, String, String > mapper = getMapper ();

        System.out.printf("%n%nComputes if present%n");

        Supplier < Map < Integer, String > > computedSupplierMap = computeIfPresent (supplierMap, mapper );

        printMap ( computedSupplierMap );

        supplierMap = getSupplierMap ();

        printMap ( supplierMap );

        System.out.printf("%n%nComputes if absent%n");

        Function < Integer, String > fMapper = getFunctionMapper ();

        computedSupplierMap = computeIfAbsent ( supplierMap,fMapper );

        printMap ( computedSupplierMap );
    }

    private static Function<Integer, String> getFunctionMapper() {

        return (k) -> {
            int squared = k*k;
            String response = String.format( "Creted -> %d", squared);
            if ( k == 4) {
                response = String.format("The squared of Four is: %d", squared);
            } else if ( k == 5) {
                response = String.format("The squared of Four is: %d", squared);
            }
            return response;
        };
    }

    private static Supplier<Map<Integer, String>> computeIfAbsent (
            Supplier<Map<Integer, String>> supplierMap,
            Function<Integer, String> mapper) {

        Map < Integer, String > rSupplierMap = supplierMap.get();

        for ( Integer index : rSupplierMap.keySet() ) {
            rSupplierMap.computeIfAbsent(index, mapper);
        }

        rSupplierMap.computeIfAbsent(8, mapper);
        rSupplierMap.computeIfAbsent(9, mapper);

        return () -> rSupplierMap;
    }

    private static void printMap (Supplier< Map< Integer, String>> computedSupplierMap) {
        Map < Integer, String > supplierMap = computedSupplierMap.get();

        System.out.println();
        for (Integer index : supplierMap.keySet() ) {
            System.out.printf("%nThe key %d has the value of: %s", index, supplierMap.get(index));
        }
        System.out.printf("%n-- - -- - -- - - -- -%n");
    }

    private static Supplier < Map < Integer, String > >
            computeIfPresent (
                    Supplier < Map < Integer, String > > supplierMap,
                    BiFunction < Integer, String, String > mapper
    ) {

        Map < Integer, String > rSupplierMap = supplierMap.get();

        for ( Integer index : rSupplierMap.keySet() ) {
            rSupplierMap.computeIfPresent(index, mapper);
        }

        return () -> rSupplierMap;
    }

    private static BiFunction < Integer, String, String >
            getMapper () {

        return ( k, v ) -> {
            //System.out.printf("%nv: %s%n", v);
            if ( v.isEmpty() ) {
                return String.format("The squared of empty is: empty");
            }else
                return String.format("The squared of %s is: %d", v, k * k);
        };
    }

    private static Supplier < Map < Integer, String > >
            getSupplierMap () {

        Map < Integer, String > tMap = new HashMap<>();

        tMap.put(0, "Zero");
        tMap.put(1, "One");
        tMap.put(2, "Two");
        tMap.put(3, "Three");
        tMap.put(4, null);
        tMap.put(5, null);
        tMap.put(6, "");
        tMap.put(7, "");

        return () -> tMap;
    }
}
