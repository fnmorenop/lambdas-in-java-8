package ocp8_functional_programming.p171_04_functional_programming.p172_using_variables_in_lambdas;

public interface Gorilla {
    String move ();
}
