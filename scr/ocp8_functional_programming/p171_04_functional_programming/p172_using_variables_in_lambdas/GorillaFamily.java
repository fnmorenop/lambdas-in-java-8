package ocp8_functional_programming.p171_04_functional_programming.p172_using_variables_in_lambdas;

public class GorillaFamily {

    String walk = "walk";

    void everyonePlay (boolean baby) {

        String approach = "amble";
        //approach = "run";

        play ( () -> walk);
        play ( () -> baby ? "hitch a ride" : "run");
        play ( () -> approach);
    }

    void play (Gorilla g) {
        System.out.printf("%ng.move: %s%n", g.move());
    }
}
