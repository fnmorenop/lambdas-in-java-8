package ocp8_functional_programming.p171_04_functional_programming.p173_working_with_builtin_functional_interfaces;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.function.Supplier;

public class P174_SupplierInterface {

    public static void main(String[] args) {

        {
            Supplier<LocalDate> s1 = LocalDate::now;
            Supplier<LocalDate> s2 = () -> LocalDate.now();

            LocalDate d1 = s1.get();
            LocalDate d2 = s2.get();

            System.out.printf("%nd1: %s%n", d1);
            System.out.printf("d2: %s%n", d2);
        }

        {
            Supplier<StringBuilder> s1 = StringBuilder::new;
            Supplier<StringBuilder> s2 = () -> new StringBuilder();

            System.out.printf("%ns1: %s%n", s1);
            System.out.printf("s2: %s%n", s2);
        }

        {
            Supplier<ArrayList<String>> s1 = ArrayList::new;
            ArrayList<String> a1 = s1.get();
            System.out.printf("%na1: %s%n", a1);
            System.out.printf("s1: %s%n", s1);
        }
    }

}
