package ocp8_functional_programming.p171_04_functional_programming.p173_working_with_builtin_functional_interfaces;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class P175_ImplementingConsumerAndBiConsumer {

    public static void main(String[] args) {

        {
            Consumer<String> c1 = System.out::print;
            Consumer<String> c2 = (x) -> System.out.printf("%nThe x values is: %s%n", x);

            c1.accept("The string");
            c2.accept("The string");
        }

        {
            Map<String, Integer> map = new HashMap<>();
            BiConsumer<String, Integer> b1 = map::put;
            BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);

            b1.accept("Chicken", 7);
            b2.accept("Chick", 1);

            System.out.printf("The map: %s%n", map);
        }

        {
            Map<String, String> map = new HashMap<>();

            BiConsumer<String, String> b1 = map::put;
            BiConsumer<String, String> b2 = (k, v) -> map.put(k, v);

            b1.accept("chicken", "Cluck");
            b2.accept("Chick", "Tweep");

            System.out.printf("The map: %s%n", map);
        }
    }
}