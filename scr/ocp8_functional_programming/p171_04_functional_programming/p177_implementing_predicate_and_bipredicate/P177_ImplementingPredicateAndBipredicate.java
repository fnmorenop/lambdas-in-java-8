package ocp8_functional_programming.p171_04_functional_programming.p177_implementing_predicate_and_bipredicate;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class P177_ImplementingPredicateAndBipredicate {

    public static void main(String[] args) {

        {
            Predicate<String> p1 = String::isEmpty;
            Predicate<String> p2 = x -> x.isEmpty();

            System.out.printf("%nP1 isEmpty: %b%n", p1.test(""));
            System.out.printf("P2 isEmpty: %b%n", p2.test(""));
        }

        {
            BiPredicate<String, String> b1 = String::startsWith;
            BiPredicate<String, String> b2 = (string, prefix) -> string.startsWith(prefix);

            System.out.printf("%nThe chicken has chicken: %b%n", b1.test("chicken", "chick"));
            System.out.printf("The chicken has chicken: %b%n", b2.test("chicken", "chick"));
        }

        {
            Predicate<String> egg = s -> s.contains("egg");
            Predicate<String> brown = s -> s.contains("brown");

            BiPredicate<String, String> biEgg = (s, s2) -> s.contains(s2);

            {
                Predicate<String> brownEggs = s -> s.contains("egg") && s.contains("brown");
                Predicate<String> otherEggs = s -> s.contains("egg") && !s.contains("brown");

                System.out.printf("%nThey are browEggs: %b%n", brownEggs);
                System.out.printf("They are otherEggs: %b%n", otherEggs);

            }

            {
                Predicate<String> brownEggs = egg.and(brown);
                Predicate<String> otherEggs = egg.and(brown.negate());

                System.out.printf("%nThey are browEggs: %b%n", brownEggs);
                System.out.printf("They are otherEggs: %b%n", otherEggs);
            }
        }
    }
}
