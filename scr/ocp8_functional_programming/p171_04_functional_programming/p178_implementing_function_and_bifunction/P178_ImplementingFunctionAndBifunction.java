package ocp8_functional_programming.p171_04_functional_programming.p178_implementing_function_and_bifunction;

import java.util.function.BiFunction;
import java.util.function.Function;

public class P178_ImplementingFunctionAndBifunction {

    public static void main(String[] args) {

        {
            Function<String, Integer> f1 = String::length;
            Function<String, Integer> f2 = x -> x.length();

            System.out.printf("The length of the string1 is: %s%n", f1.apply("Cluck"));
            System.out.printf("The length of the string2 is: %s%n", f2.apply("Cluck"));
        }

        {
            BiFunction<String, String, String> b1 = String::concat;
            BiFunction<String, String, String> b2 = (string, toAdd) -> string.concat(toAdd);

            String string = "baby";
            String toAdd = "chick";

            System.out.printf("%n1. The string %s and the string %s forms: %s%n", string, toAdd, b1.apply(string, toAdd));
            System.out.printf("2. The string %s and the string %s forms: %s%n", string, toAdd, b2.apply(string, toAdd));
        }
    }
}
