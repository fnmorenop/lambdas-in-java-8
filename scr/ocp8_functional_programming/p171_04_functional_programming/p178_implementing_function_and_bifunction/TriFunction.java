package ocp8_functional_programming.p171_04_functional_programming.p178_implementing_function_and_bifunction;

public interface TriFunction<T, U, V, R> {
    R apply (T t, U u ,V v);
}
