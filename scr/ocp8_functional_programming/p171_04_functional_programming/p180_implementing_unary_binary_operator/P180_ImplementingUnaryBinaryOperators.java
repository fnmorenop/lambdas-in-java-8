package ocp8_functional_programming.p171_04_functional_programming.p180_implementing_unary_binary_operator;

import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class P180_ImplementingUnaryBinaryOperators {

    public static void main(String[] args) {

        {
            UnaryOperator<String> u1 = String::toUpperCase;
            UnaryOperator<String> u2 = x -> x.toUpperCase();

            String theString = "chirp";

            System.out.printf("%nThe string1: %s uppercase is: %s%n", theString, u1.apply(theString));
            System.out.printf("The string2: %s uppercase is: %s%n", theString, u2.apply(theString));
        }

        {
            BinaryOperator<String> b1 = String::concat;
            BinaryOperator<String> b2 = (x, toAdd) -> x.concat(toAdd);

            String theString = "baby ";
            String theToAdd = "chick";

            System.out.printf("%nThe binary operator 1: %s%n", b1.apply(theString, theToAdd));
            System.out.printf("The binary operator 2: %s%n", b2.apply(theString, theToAdd));
        }
    }
}
