package ocp8_functional_programming.p171_04_functional_programming.p181_checking_functional_interfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class P181_CheckingFunctionalInterfaces {

    public static void main(String[] args) {

        {
            Predicate<List> ex1 = x -> "".equals(x.get(0));

            List<String> list = Arrays.asList("S1", "S2", "S3", "S4", "S5");

            System.out.printf("%nThe 0 index in the list is empty: %b", ex1.test(list));
        }

        {
            Consumer<Long> ex2 = (Long l) -> System.out.printf("%nThe long value is: %,d%n", l);

            Long theLong = 123_456_789_123_456_789_0L;

            ex2.accept(theLong);
        }

        {
            BiPredicate<String, String> ex3 = (s1, s2) -> false;

            String theString1 = "S1";
            String theString2 = "S2";

            System.out.printf("%nThe BiPredicate of the values is: %b%n", ex3.test(theString1, theString2));
        }
    }
}
