package ocp8_functional_programming.p171_04_functional_programming.p181_checking_functional_interfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class P182_CheckingFunctionalErrors {

    public static void main(String[] args) {

        {
            Function<List<String>, String> ex1 = x -> x.get(0);

            List<String> theList = Arrays.asList("S1", "S2", "S3", "S4");

            System.out.printf("%nThe function's result is: %s%n", ex1.apply(theList));
        }

        {
            UnaryOperator<Long> ex2 = (Long l) -> (long) 3.14;

            Long theLong = 123_456_789_012L;

            System.out.printf("%nThe unary operator's result is: %,d%n", ex2.apply(theLong));
        }

        {
            Predicate<String> ex4 = String::isEmpty;

            String theString = "The test string";

            System.out.printf("%nThe predicate's result is: %s%n", ex4.test(theString));
        }
    }
}
