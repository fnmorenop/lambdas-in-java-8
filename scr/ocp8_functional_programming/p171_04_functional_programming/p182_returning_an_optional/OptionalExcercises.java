package ocp8_functional_programming.p171_04_functional_programming.p182_returning_an_optional;

import java.util.*;
import java.util.function.Function;

public class OptionalExcercises {
    public static void main(String[] args) {

        Optional<Function<String, Integer>> functionOptional =
                Optional.of(String::length);

        functionOptional.ifPresent((f) -> System.out.printf("functionalOptional.apply(\"theString\"):%d%n",
                f.apply("TheString")));

        System.out.printf("%nfunctionOptional: %s%n", functionOptional.get().toString());

        Optional<Function<String, Integer>> emptyFunctionOptional = Optional.empty();

        //System.out.printf("%emptyFunctionOptional: %d%n",
          //      emptyFunctionOptional.orElse((Optional.ofNullable( new Integer(0)))));

        System.out.println(emptyFunctionOptional);

        //int sum;

        Optional<Function<List<Integer>, Double>> average =
                Optional.ofNullable((List<Integer> l) -> {
                    Integer sum = 0;
                    for (Integer i : l) sum += i;
                    return (double) sum / l.size();
                });

        List<Integer> numList = Arrays.asList(100, 100, 90, 80);

        //List<Integer> numList = new ArrayList<>();

        //List<Integer> numList = null;

        //System.out.printf("%nThe average is %.2f%n", average.get().apply(numList));

        average.ifPresent( l -> System.out.printf("%nThe average is %.2f%n",
                average.get().apply(numList)));

        System.out.println("-------------");
        System.out.println("Optional methods");

        Double doubleValue = 3d;
        doubleValue = null;

        System.out.printf("%ndoubleValue:%.0f%n", doubleValue);

        Optional<Double> doubleOptional = Optional.ofNullable(doubleValue);

        //doubleOptional = Optional.empty();

        System.out.printf("%ndoubleOptional.orElse(5d): %.0f%n", doubleOptional.orElse(5d));
        System.out.printf("doubleOptional.orElseGet(() -> Math.random()): %.0f%n",
                doubleOptional.orElseGet(() -> Math.random()));
        try {
            System.out.printf("doubleOptional.orElseThrow(() -> new NumberFormatException(\"no number\")): %s%n",
                    doubleOptional.orElseThrow(() -> new NumberFormatException("no number")));
        }catch (NumberFormatException e) {
            System.out.println("Exeption: It should exist a valid number");
            e.printStackTrace();
        }
        //System.out.printf("", doubleOptional.orElseGet(() -> new Exception("No exception"))); ->
          // Doesn't not compile as
    }
}
