package ocp8_functional_programming.p171_04_functional_programming.p182_returning_an_optional;

import java.util.Optional;

public class P182_AverageMethod {

    public static void main(String[] args) {

        {
            System.out.printf("%nThe average between 90 and 100 is: %s%n", average(90, 100));
            System.out.printf("The empty average is: %s%n", average());
        }

        {
            Optional<Double> opt = average(90, 100);

            //System.out.printf("%nopt: %s%n", opt);

            if (opt.isPresent()) {
                System.out.printf("%nThe average is: %s%n", opt.get());
            }
        }

        {
            Optional<Double> opt = average();

            System.out.printf("%nThe opt equals to: %s%n", opt);
        }

    }

    public static Optional<Double> average (int... scores) {

        if (scores.length == 0) {
            return Optional.empty();
        }

        int sum = 0;

        for (int score : scores) {
            sum += score;
        }

        return Optional.of((double) sum / scores.length);
    }
}
