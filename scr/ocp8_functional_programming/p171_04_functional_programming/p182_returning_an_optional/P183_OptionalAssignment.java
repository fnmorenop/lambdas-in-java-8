package ocp8_functional_programming.p171_04_functional_programming.p182_returning_an_optional;

import java.util.Optional;

public class P183_OptionalAssignment {

    public static void main(String[] args) {

        String value = "";

        {
            Optional o = (value == null) ? (Optional.empty()) : (Optional.of(value));

            System.out.printf("%nThe optional value is: %s%n", o);
        }

        {
            Optional o = Optional.ofNullable(value);

            System.out.printf("%nThe optional value is: %s%n", o);
        }


    }
}
