package ocp8_functional_programming.p171_04_functional_programming.p182_returning_an_optional;

import java.util.Optional;

public class P184_OptionalMethods {

    public static void main(String[] args) {

        {
            Optional<Double> opt = P182_AverageMethod.average(90, 100);
            opt.ifPresent(System.out::println);
        }
    }
}
