package ocp8_functional_programming.p171_04_functional_programming.p188_creating_stream_sources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class P188_Creating_Stream_Sources {
    public static void main(String[] args) {

        Stream<String> empty = Stream.empty(); // count = 0
        Stream<Integer> singleElement = Stream.of(1); // count = 1
        Stream<Integer> fromArray = Stream.of(1, 2, 3); // count = 2

        Stream<String> streamStringArray = Stream.of("One", "Two", "Three");

        Stream<Function<List<String>, Integer>> streamFunction = Stream.of(List::size);

        List<String> list = Arrays.asList("a", "b", "c");
        Stream<String> fromList = list.stream();
        Stream<String> fromListParallel = list.parallelStream();

        Stream<List<String>> listStringStream = Stream.of(Arrays.asList("1", "2", "3"));
        Stream<String> stringStream = Arrays.asList("1", "2", "3").stream();
        Stream<String> paralellStringStream = Arrays.asList("1", "2", "3").parallelStream();

        System.out.println("Generate");
        Stream<Double> randoms = Stream.generate(Math::random);
        randoms.limit(100).parallel().forEach(System.out::println);

        System.out.println("Iterate");
        Stream<Integer> oddNumbers = Stream.iterate(0, n -> n + 2).limit(100).parallel();
        oddNumbers.forEach(System.out::println);

        System.out.println("generate uses");
        Stream<Integer> twoStream = Stream.generate(() -> 2).limit(5);
        twoStream.forEach(System.out::println);

        System.out.println("Iterate: accUses");
        Stream<Integer> accUses = Stream.iterate(0, i -> i + 1).limit(100);
        accUses.forEach(System.out::println);

        System.out.println("Pararell Iterate Unordered List");
        accUses = Stream.iterate(0, i -> i + 1).parallel().limit(100).unordered();
        List<Integer> integerList = accUses.collect(Collectors.toList());
        System.out.printf("%nintegerList:%s%n", integerList);

        String theString = "theCharString";
        System.out.printf("%ntheString: %s%n", theString);

        char[] charDefinition = {'a', 'b', 'c'};
        System.out.printf("%ncharDefinition: %s%n", charDefinition);
        char[] theCharArray = new char[theString.length()];
        theString.getChars(0, theString.length(), theCharArray, 0);
        System.out.printf("%ntheCharArray: %s%n", theCharArray);

        {/*
            Stream<Character> characterStream = Stream.iterate(theCharArray[0],
                    i -> new Character(theCharArray[i + 1])).limit(2);//theCharArray.length - 1);
            List<Character> characterListStream = characterStream.collect(Collectors.toList());
            System.out.printf("%ncharacterListStream: %s%n", characterListStream);
        */}

        {
            IntStream intStream = theString.chars();
            Collector<Integer, StringBuffer, StringBuffer> collector = Collector.of(
                    () -> new StringBuffer(),
                    (StringBuffer sb, Integer e) -> {
                        sb.append("<").append(/*(char)*/ e.intValue()).append(">");
                        System.out.printf("<--%s-->", Integer.toBinaryString(e));
                        //return;
                        },
                    (StringBuffer sbOld, StringBuffer sbNew) -> {
                        return sbOld.append(sbNew);
                    }
                    );
            System.out.printf("%nintStream: %s%n", intStream.boxed().collect(collector));
            //intStream.forEach(System.out::println);
            System.out.println();

            intStream = theString.chars();
            Collector<Integer, StringBuilder, StringBuilder> charOrderCollector = Collector.of(
                    StringBuilder::new,
                    (StringBuilder sb, Integer e) -> {
                        char charElement = (char) e.intValue();
                        //System.out.printf(">-- sb: %s  >-- element: %c%n", sb, charElement);
                        if(sb.length() < 1) {
                            //System.out.println("0-- Sb.lenght = 0");
                            //System.out.printf("sb: %s%n", sb);
                            sb.append(charElement);
                            //System.out.printf("sb: %s%n", sb);
                        } /*else if (sb.length() == 1){
                            int index = 0;
                            System.out.printf("1-- sb.charAt(%d) < charElement --> %c < %c --> %b%n", index, sb.charAt(index), charElement, sb.charAt(index) < charElement);
                            if(sb.charAt(0) <  charElement) {
                                sb.insert(0, charElement);
                            }else {
                                sb.append(charElement);
                            }
                        }*/ else /*if(sb.length() >= 1)*/{
                            //System.out.printf("2--00-- sb.length() > 1: --> %d > 1 --> %b%n", sb.length(), sb.length() > 1);
                            //System.out.printf("2--01-- sb.charAt(0) <= charElement: --> %c <= %c --> %b%n", sb.charAt(0), charElement, sb.charAt(0) <= charElement);
                            //System.out.printf("2--02-- sb.charAt(sb.length() -1) >= charElement -> %c <= %c --> %b%n", sb.charAt(sb.length() -1), charElement, sb.charAt(sb.length() -1) >= charElement);
                            if(sb.charAt(0) <= charElement) {
                                sb.insert(0, charElement);
                            }else if (sb.charAt(sb.length() - 1) >= charElement){
                                sb.append(charElement);
                            }else {
                                int index = 0;
                                for (int i = 1; i < sb.length(); i++) {
                                    //System.out.printf("2--03-- i: %d, index: %d, sb.length(): %d, sb: %s%n", i, index, sb.length(), sb);
                                    //System.out.printf("2--03-- sb.charAt(index++) >= charElement && charElement >= sb.charAt(i) --> %c >= %c && %c >= %c --> %b && %b --> %b%n",
                                      //      sb.charAt(index), charElement, charElement, sb.charAt(i), sb.charAt(index) >= charElement , charElement >= sb.charAt(i), sb.charAt(index) >= charElement && charElement >= sb.charAt(i));

                                    if(sb.charAt(index++) >= charElement && charElement >= sb.charAt(i)) {
                                        sb.insert(i, charElement);
                                        break;
                                    }
                                }
                            }
                        }
                        //System.out.printf("sb: %s, sb.length(): %d%n", sb, sb.length());
                        //System.out.println();
                    },
                    (sbOld, sbNew) -> sbNew.append(sbOld) //sbOld.append(sbNew)
            );
            System.out.printf("%ntheStreamOrdered: %s%n", intStream.boxed().collect(charOrderCollector));
        }

        IntStream intStream = theString.chars();
        Collector<Integer, StringBuilder, StringBuilder> charOrderCollector = Collector.of(
                StringBuilder::new,
                (StringBuilder sb, Integer elem) -> {
                    Character c = (char) elem.intValue();
                    //System.out.printf("sb: %s, e: %c%n", sb, c);
                    if (sb.length() < 1) sb.append(c);
                    else if (c > sb.charAt(0)) sb.insert(0, c);
                    //else if (c < sb.charAt(sb.length() - 1) ) sb.append(c);
                    else for (int i = 0; i + 1 < sb.length(); i++)
                        if (c <= sb.charAt(i) && c >= sb.charAt(i+1)){
                            sb.insert(i+1, c);
                            break;
                        }
                },
                (sbOld, sbNew) -> sbOld.append(sbNew)
        );
        System.out.printf("%ntheStreamOrderedCleansed: %s%n", intStream.boxed().collect(charOrderCollector));

        List<Character> characterList = new ArrayList<>();
        for(Character c : theCharArray) {
            characterList.add(c);
        }

        System.out.printf("characterList: %s%n", characterList);

       Stream<Character> characterStream = characterList.stream().parallel().sequential();

       //characterList.forEach(System.out::println);

       characterStream.filter(c -> c.hashCode() >= 65 && c.hashCode() <= 90).forEach(System.out::println);


    }
}
