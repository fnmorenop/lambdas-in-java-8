package ocp8_functional_programming.p171_04_functional_programming.p189_using_common_terminal_operations;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P189_UsingCommonTerminalOperations {

    public static void main(String[] args) {

        System.out.println("<<<   Stream Terminal Methods\n");

        System.out.println("<<<   Count() method");

        Stream<String> s = getStream();
        System.out.printf("%ns.length: %d%n%n", s.count());

        System.out.println("<<<   min() and max() methods");

        s = getStream();
        //Optional<String> min = s.min((s1, s2) -> s1.length() - s2.length());
        Optional<String> min = s.min(Comparator.comparingInt(String::length));
        min.ifPresent(minor -> System.out.printf("%nminor: %s%n", minor));

        System.out.printf("%n<<<   Stream.min empty return option%n");

        Optional<?> maxEmpty = Stream.empty().max((s1, s2) -> 0);
        System.out.printf("%nmaxEmpty.isPresent(): %b%n", maxEmpty.isPresent());

        System.out.printf("%n<<<   findAny() and findFirst()%n");

        s = getStream().parallel();
        System.out.printf("%n%n");
        getStream().forEach(el -> System.out.printf(": %s :",el));
        System.out.printf("%n%n");
        Stream<String> infinite = Stream.generate(() -> "chimp").parallel().limit(1000);
        s.findAny().ifPresent(sResult -> System.out.printf("%nfindAny result: %s%n", sResult));
        infinite.findAny().ifPresent(infiniteResult -> System.out.printf("%nfindAny infiniteResult: %s%n", infiniteResult));
        s = getStream();
        s.findFirst().ifPresent(sResult -> System.out.printf("%nfindFirst result: %s%n", sResult));
        infinite = Stream.generate(() -> "chimp").limit(1000);
        infinite.findFirst().ifPresent(infiniteResult -> System.out.printf("%nfindFirst infiniteResult: %s%n", infiniteResult));

        System.out.printf("%n<<<   allMatch(), anyMatch() and noneMatch()%n");

        List<String> list = Arrays.asList("monkey", "2", "chimp");
        infinite = Stream.generate(() -> "chimp").parallel().limit(1000);
        Predicate<String> pred = x -> Character.isLetter(x.charAt(0));
        System.out.printf("anyMatch: %b%n", list.stream().parallel().anyMatch(pred));
        System.out.printf("allMatch: %b%n", list.stream().parallel().allMatch(pred));
        System.out.printf("noneMatch: %b%n", list.stream().parallel().noneMatch(pred));
        System.out.printf("infinite.anyMatch: %b%n", infinite.anyMatch(pred));

        System.out.printf("%n<<<   forEach()%n");

        s = getStream().parallel();
        System.out.printf("%n%n");
        getStream().forEach(el -> System.out.printf(": %s :",el));
        System.out.printf("%n%n");

        s = Stream.of("1");
        //for (String st : s) {} // Doesn't compile as the stream doesn't implement the iterable interface

        System.out.printf("%n<<<   reduce()%n");

        String[] array = new String[] {"w", "o", "l", "f"};
        String result = "";
        for (String st : array)
            result += st;
        System.out.printf("%narray result: %s%n", result);

        s = Stream.of(array);
        result = s.reduce("",(string, character) -> string + character);
        System.out.printf("%nStream result: %s%n", result);
        s = Stream.of(array);
        StringBuilder
                sbResult = s.
                map(StringBuilder::new).
                reduce(StringBuilder::append).get();

        System.out.printf("%nStringBuilder Stream sbResult: %s%n", sbResult);
        s = Stream.of(array);
        result = s.reduce("",String::concat);
        System.out.printf("%nStream method reference result: %s%n", result);

        Stream<Integer> i = Stream.iterate(1, n -> n + 1).limit(10);
        Integer iResult = i.reduce(1, Math::multiplyExact);
        System.out.printf("%niResult: %,d%n", iResult);


        System.out.printf("%nCharacter Stream %n");
        Stream<Character> characterStream =
                Stream.
                        iterate(65, c -> c + 1).
                        parallel().
                        limit(57).
                        map(n -> (char) n.intValue()).
                        filter(el -> Character.isLetter(el));

        characterStream.forEach(e -> System.out.printf("%c", e));
        System.out.println();

        System.out.printf("%nOptional return%n");

        BinaryOperator<Integer> op = Math::multiplyExact;
        Consumer<Integer> printer = el -> System.out.printf("%d%n", el);
        Stream<Integer> empty = Stream.empty();
        Stream<Integer> oneElement = Stream.of(3);
        Stream<Integer> threeElements = Stream.of(3, 5, 6);

        empty.reduce(op).ifPresent(printer);
        oneElement.reduce(op).ifPresent(printer);
        threeElements.reduce(op).ifPresent(printer);

        threeElements = Stream.of(3, 5, 6);
        System.out.printf("%nParallel Reduce: %d%n", threeElements.reduce(1, op, op));

        System.out.printf("%n<<<   collect()%n%n");

        Supplier<Stream<String>> wolf = () -> Stream.of("w", "o", "l", "f");
        StringBuilder sbWord =
                wolf.
                        get().
                        collect(
                                StringBuilder::new,
                                StringBuilder::append,
                                StringBuilder::append);

        System.out.printf("%nsbWord: %s%n", sbWord);

        TreeSet<String> theTreeSet =
                            wolf.get().
                            collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
        System.out.printf("%ntheTreeSet: %s%n", theTreeSet);

        theTreeSet =
                wolf.get().
                        collect(TreeSet::new, (t, e) -> t.add(String.format(":%s:", e)), TreeSet::addAll);
        System.out.printf("%ntheTreeSet formated: %s%n", theTreeSet);

        Set<String> theSet =
                wolf.get().
                        collect(Collectors.toSet());

        System.out.printf("%ntheSet: %s%n", theSet);

        List<String> theList =
                wolf.get().
                        collect(Collectors.toList());

        System.out.printf("%ntheList: %s%n", theList);



    }

    private static Stream<String> getStream() {
        return Stream.of("Monkey", "Gorilla", "Bonono", "Ape");
    }
}
