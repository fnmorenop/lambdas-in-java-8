package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class P196_Distinct {

    public static void main(String[] args) {

        Supplier<Stream<String>> animalSupplier =
                () -> Stream.of("duck", "duck", "duck", "goose");
        Consumer<String> consumer = s -> System.out.printf(":%s:%n", s);

        animalSupplier.
                get().
                distinct().
                forEach(consumer);


    }
}
