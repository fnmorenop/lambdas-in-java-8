package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class P196_Filter {

    public static void main(String[] args) {

        Supplier<Stream<String>> animalsSupplier =
                () -> Stream.of("Monkey", "Gorilla", "Bonono", "Ape");
        Predicate<String> predicate =
                s -> s.startsWith("M");
        Consumer<String> consumer =
                s -> System.out.printf(":%s:%n", s);

        animalsSupplier.
                get().
                filter(predicate).
                forEach(consumer);


    }
}
