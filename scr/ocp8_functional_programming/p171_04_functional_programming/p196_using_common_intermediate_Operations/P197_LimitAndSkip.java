package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class P197_LimitAndSkip {

    public static void main(String[] args) {

        Consumer<Integer> printer = s -> System.out.printf(":%d:", s);
        UnaryOperator<Integer> iterador = n -> n + 1;

        Stream<Integer> s = Stream.iterate(60, iterador);
        s.skip(100).limit(40).forEach(printer);
    }
}
