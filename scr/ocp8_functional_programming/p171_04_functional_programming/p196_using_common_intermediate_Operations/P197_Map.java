package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class P197_Map {

    public static void main(String[] args) {

        Supplier<Stream<String>> animalsSupplier =
                () -> Stream.of("Monkey", "Gorilla", "Bonono", "Ape");
        Function<String, Integer> wordLengthMapper =
                String::length;
        Consumer<Integer> print =
                s -> System.out.printf(":%d:", s);

        animalsSupplier.
                get().
                map(wordLengthMapper).
                forEach(print);


    }
}
