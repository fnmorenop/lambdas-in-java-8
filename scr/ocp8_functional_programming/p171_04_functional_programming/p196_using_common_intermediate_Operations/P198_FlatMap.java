package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class P198_FlatMap {

    public static void main(String[] args) {

        List<String> zero = Arrays.asList();
        List<String> one = Arrays.asList("Bonono");
        List<String> two = Arrays.asList("Mamá Gorilla", "Baby Gorilla");
        Stream<List<String>> animals = Stream.of(zero, one, two);

        Function<List<String>, Stream<String>> listToStream =
                List::stream; // list -> list.stream();
        Consumer<String> printer =
                s -> System.out.printf(":%s:%n", s);

        Consumer<List<String>> listPrinter =
                l -> System.out.printf(":%s:%n", l);

        animals.forEach(listPrinter);

        System.out.println();
        animals = Stream.of(zero, one, two);
        animals.flatMap(listToStream).forEach(printer);
    }
}
