package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class P198_Sorted {

    public static void main(String[] args) {

        Supplier<Stream<String>> animalsSupplier =
                () -> Stream.of("Monkey", "Gorilla", "Bonono", "Ape", "Simian");

        Consumer<String> print =
                s -> System.out.printf(":%s:", s);

        System.out.printf("%nAnimals%n");
        animalsSupplier.
                get().
                forEach(print);

        System.out.printf("%n%nSorted Animals%n");
        animalsSupplier.
                get().
                sorted().
                forEach(print);

        System.out.printf("%n%nSorted Reversed Animals%n");
        animalsSupplier.
                get().
                sorted(Comparator.reverseOrder()).
                forEach(print);

        System.out.printf("%n%nSorted Reversed method reference Animals%n");
        animalsSupplier.
                get().
                //sorted(Comparator::reverseOrder).  // Doesn't compile as reverse order doesn't take parameters
                forEach(print);

    }
}
