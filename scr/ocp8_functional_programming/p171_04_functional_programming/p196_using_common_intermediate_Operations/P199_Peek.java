package ocp8_functional_programming.p171_04_functional_programming.p196_using_common_intermediate_Operations;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P199_Peek {

    public static void main(String[] args) {

        {
            Consumer<String> print =
                    s -> System.out.printf(":%s:", s);

            Consumer<String> printFilter =
                    s -> System.out.printf("<%s>", s);

            Supplier<Stream<String>> animalsSupplier =
                    () -> Stream.of("Monkey", "Gorilla", "Bonono", "Ape", "Simian");

            Predicate<String> gFilter =
                    s -> s.startsWith("G") | s.startsWith("B");

            System.out.printf("%n%nAnimals%n");
            animalsSupplier.
                    get().
                    forEach(print);

            System.out.printf("%n%nPeeked Animals%n");
            long count = animalsSupplier.
                    get().
                    //peek(print).
                            filter(gFilter).
                            peek(printFilter).
                            count();
            System.out.printf("%ncount: %d%n", count);
        }

        {
            int boundary = 10;
            List<Integer> numbers = Stream.
                    iterate(1, n -> n + 1).
                    limit(boundary).
                    collect(Collectors.toList()); //new ArrayList<>();
            List<Character> letters = Stream.
                    iterate('a', c ->  (char) (c + 1)).
                    limit(boundary).
                    collect(Collectors.toList());// new ArrayList<>();
            //numbers.add(1);
            //letters.add('a');

            Supplier<Stream<List<?>>> stream = () -> Stream.of(numbers, letters);

            System.out.printf("%n%nUsing peek%n");


            Consumer<Integer> print =
                    s -> System.out.printf(":%d:", s);

            Function<List<?>, Integer> mapperToSize =
                    List::size; //l -> l.size();

            System.out.printf("%n%nNormal mapping%n%n");
            stream.
                    get().
                    map(mapperToSize).
                    forEach(print);

            System.out.printf("%n%nPeeking without changing the Stream State%n%n");
            StringBuilder builder = new StringBuilder();
            Consumer<List<?>> listPeeker = builder::append; //l -> builder.append(l);
            stream.
                    get().
                    peek(listPeeker).
                    map(mapperToSize).
                    forEach(print);
            System.out.printf("%nbuilder: %s%n", builder);

            System.out.printf("%n%nPeeking changing the Stream State%n%n");
            Consumer<List<?>> listRemover = l -> l.removeAll(l);
                    stream.
                            get().
                            peek(listRemover).
                            map(mapperToSize).
                            forEach(print);

        }
    }
}
