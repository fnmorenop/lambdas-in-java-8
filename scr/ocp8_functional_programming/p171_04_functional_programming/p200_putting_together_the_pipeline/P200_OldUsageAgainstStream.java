package ocp8_functional_programming.p171_04_functional_programming.p200_putting_together_the_pipeline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class P200_OldUsageAgainstStream {

    public static void main(String[] args) {

        System.out.printf("%n%nOld Usage: filtered order list%n%n");
        printOldUsage();

        System.out.printf("%n%nNEW Usage: filtered order list%n%n");
        printNewUsage();
    }

    private static void printNewUsage() {

        Predicate<String> isStringEqualsTo4 =
                s -> s.length() == 4;
        int boundary = 2;
        Consumer<String> printString =
                s -> System.out.printf(":%s:", s);

        getNames().
                stream().
                filter(isStringEqualsTo4).
                sorted().
                limit(boundary).
                forEach(printString);
    }

    private static void printOldUsage() {

        List<String> list = getNames();
        List<String> filtered =
                new ArrayList<>();

        for (String name : list)
            if (name.length() == 4)
                filtered.add(name);

        Collections.sort(filtered);

        int resultNumber = 2;
        for (int i = 0; i < resultNumber; i++)
            System.out.printf(":%s:", filtered.get(i));
    }

    private static List<String> getNames() {
        return Arrays.asList("Toby", "Anna", "Leroy", "Alex");
    }
}
