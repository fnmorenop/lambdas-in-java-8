package ocp8_functional_programming.p171_04_functional_programming.p200_putting_together_the_pipeline;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class P202_PipelineExamples {

    public static void main(String[] args) {

        Predicate<String> isStringEqualsTo4 =
                s -> s.length() == 4;
        int boundary = 2;
        Consumer<String> printString =
                s -> System.out.printf(":%s:", s);

        System.out.printf("%n%nUnsorted stream%n%n");
        Stream.
                generate(() -> "Elsa").
                filter(isStringEqualsTo4).
                //sorted(). // OutOfMemoryError
                limit(boundary).
                forEach(printString);

        System.out.printf("%n%nSorted stream%n%n");
        Stream.
                generate(() -> "Elsa").
                filter(isStringEqualsTo4).
                limit(boundary).
                sorted().
                forEach(printString);

        /* Hangs up
        System.out.printf("%n%nSorted stream empty%n%n");
        Stream.
                generate(() -> "Olaf Lazisson").
                filter(s -> s.length() == 4).
                limit(2).
                sorted().
                forEach(printString);
*/
        System.out.printf("%n%nSorted stream empty%n%n");
        Stream.
                generate(() -> "Olaf Lazisson").
                limit(2).
                filter(s -> s.length() == 4).
                sorted().
                forEach(printString);
    }
}
