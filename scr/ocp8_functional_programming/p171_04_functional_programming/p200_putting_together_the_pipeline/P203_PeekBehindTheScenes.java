package ocp8_functional_programming.p171_04_functional_programming.p200_putting_together_the_pipeline;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class P203_PeekBehindTheScenes {

    public static void main(String[] args) {

        Consumer<Integer> printInteger =
                s -> System.out.printf(":%d:", s);

        System.out.printf("%n%nLimited odd numbers%n%n");
        Stream.
                iterate(1, n -> n + 1).
                limit(15).
                filter(n -> n % 2 == 1).
                forEach(printInteger);

        System.out.printf("%n%nPeeked Limited odd numbers%n%n");
        Stream.
                iterate(1, n -> n + 1).
                limit(15).
                peek(printInteger).
                filter(n -> n % 2 == 1).
                //peek(printInteger).
                forEach(printInteger);

        System.out.printf("%n%nFiltered Limited odd numbers%n%n");
        Stream.
                iterate(1, n -> n + 1).
                filter(n -> n % 2 == 1).
                limit(15).
                forEach(printInteger);

        System.out.printf("%n%nPeaked Filtered Limited odd numbers%n%n");
        Stream.
                iterate(1, n -> n + 1).
                //peek(printInteger).
                filter(n -> n % 2 == 1).
                peek(printInteger).
                limit(15).
                //peek(printInteger).
                forEach(s -> {}); //printInteger);
    }
}
