package ocp8_functional_programming.p171_04_functional_programming.p204_printing_a_stream;

import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P204_StreamPrinter {

    public static void main(String[] args) {

        Consumer<String> printString =
                s -> System.out.printf(":%s:", s);

        getInfiniteStream().
                limit(10).
                forEach(printString);

        System.out.printf("%n%nStream: %s%n%n",
                getInfiniteStream().
                limit(10).
                collect(Collectors.toList())
        );

        getInfiniteStream().
                limit(10).
                peek(printString).
                count();
    }

    private static Stream<String> getInfiniteStream() {
        return Stream.
                iterate(-10, n -> n + 1).
                map(n -> Integer.toString(n));
    }
}
