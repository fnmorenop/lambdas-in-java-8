package ocp8_functional_programming.p171_04_functional_programming.p205_working_with_primitives;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class P205_CreatingPrimitiveStrings {

    public static void main(String[] args) {

        int limit = 10;

        DoubleConsumer printDouble =
                s -> System.out.printf(":%,.2f:", s);

        IntConsumer printInteger =
                s -> System.out.printf(":%,d:", s);

        DoubleStream empty = DoubleStream.empty();

        DoubleStream oneValue = DoubleStream.of(3.14);
        DoubleStream varargs = DoubleStream.of(1.0, 1.1, 1.2);

        System.out.printf("%nDoubleStream oneValue%n");
        oneValue.forEach(printDouble);

        System.out.printf("%nDoubleStream varArgs%n");
        varargs.forEach(printDouble);

        System.out.printf("%nDoubleStream Ramdom numbers%n");
        DoubleStream.
                generate(Math::random).
                parallel().
                limit(limit).
                sorted().
                forEach(printDouble);

        System.out.printf("%nDoubleStream  Fractions numbers%n");
        DoubleStream.
                iterate(0.5, d -> d / 2).
                //parallel().
                limit(limit).
                //sorted().
                forEach(printDouble);

        System.out.printf("%nRamdom stream%n");
        new Random().
                doubles().
                limit(limit).
                forEach(printDouble);

        System.out.printf("%nRange stream%n");
        IntStream.
                range(-1, 6).
                forEach(printInteger);

        System.out.printf("%nClosed Range stream%n");
        IntStream.
                rangeClosed(-1, 6).
                forEach(printInteger);

        System.out.printf("%nClosed Char Range stream%n");
        IntConsumer printChar = c -> System.out.printf(":%c:", c);
        IntStream.
                rangeClosed('a', 'z').
                forEach(printChar);


        System.out.printf("%nInverted Closed Char Range stream%n");
        List<Integer> numbs =  IntStream.rangeClosed(1, 10).collect(
                () -> new ArrayList<Integer>(),
                List::add,
                List::addAll
        );

        numbs.
                stream().
                forEach(e -> System.out.printf(":%d:", e));
    }
}
