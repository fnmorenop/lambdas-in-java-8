package ocp8_functional_programming.p171_04_functional_programming.p205_working_with_primitives;

import java.util.function.Supplier;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class P205_MappingStreams {

    public static void main(String[] args) {

        Supplier<Stream<Object>> objectStream = () ->
                Stream.of("Object1", "Object0", "Object3", "Object4", "Object5", "Object6");

        System.out.printf("%nObjectStream to StringStream%n");
        Supplier<Stream<String>> stringStream = () -> objectStream.get().map(e -> e.toString());
        stringStream.get().forEach(e -> System.out.printf(":%s:", e));

        System.out.printf("%n%nStringStream to doubleStream%n");
        DoubleStream doubleStream = stringStream.get().mapToDouble(e -> e.length());
        doubleStream.forEach(e -> System.out.printf(":%,.0f:", e));

        System.out.printf("%n%nStringStream to intStream%n");
        IntStream intStream = stringStream.
                get().
                mapToInt(
                        e -> Integer.valueOf(String.valueOf(e.charAt(e.length() - 1)))
                );
        intStream.peek(i -> System.out.printf(":%d:", i)).count();

        System.out.printf("%n%nStringStream to LongStream%n");
        LongStream longStream = stringStream.
                get().
                mapToLong(e -> e.charAt(e.length() - 1));
        longStream.forEach(e -> System.out.printf(":%d:", e));

        System.out.printf("%n%nStringStream to LongStream%n");
        DoubleStream doubleStream1 = stringStream.
                get().parallel().
                //map(e -> String.valueOf(e.charAt(e.length() - 1))).
                map(e -> "" + e.charAt(e.length() - 1)).
                mapToInt(Integer::valueOf).
                mapToDouble(e -> e);
        doubleStream1.forEach(e -> System.out.printf(":%,.2f:", e));

        System.out.printf("%n%nObjectStream to SubStringStream%n");
        Stream stream = objectStream.
                get().
                map(s -> s.toString()).
                map(s -> s.substring(s.length() - 1)).
                mapToInt(Integer::valueOf).
                sorted().
                mapToObj(String::valueOf);
        stream.forEach(e -> System.out.printf(":%s:", e));

        System.out.printf("%n%nString to IntStream%n");
        IntStream intStream1 = "The String".chars();
        intStream1.
                sorted().
                forEach(e -> System.out.printf("%c", e));

        System.out.printf("%n%nString to Sorted String -> collecting%n");
        String theSortedString = "The Tested String".
                chars().
                sorted().
                mapToObj(e -> String.valueOf((char) e)).
                collect(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append
                ).toString();
        System.out.printf("theSortedString: %s", theSortedString);

        System.out.printf("%n%nString to Sorted String -> reducing%n");
        theSortedString = "The Tested String".
                chars().
                sorted().
                mapToObj(e -> String.valueOf((char) e)).
                reduce(String::concat).
                get();
        System.out.printf("theSortedString: %s", theSortedString);
    }
}
