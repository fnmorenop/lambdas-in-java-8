package ocp8_functional_programming.p171_04_functional_programming.p205_working_with_primitives;

import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P205_WorkingWithPrimitives {

    public static void main(String[] args) {

        Consumer<Integer> printInteger =
                s -> System.out.printf(":%,d:", s);

        int limit = 10;

        System.out.printf("%nSum using Reduce Stream: %,d%n",
                getStream(limit).reduce(0, (Acc, el) -> Acc + el)
        );

        System.out.printf("%nSum mapping to intStream: %,d%n",
                getStream(limit).
                        mapToInt(n -> n).
                        sum()
        );

        System.out.printf("%nAverage using intStream: %,.2f%n",
                getStream(limit).
                        mapToInt(n -> n).
                        average().
                        getAsDouble()
        );

        //Stream.of(1).collect(Collectors.toList())

    }

    private static Stream<Integer> getStream(Integer limit) {
        return Stream.iterate(1, n -> n + 1).limit(limit);
    }
}
