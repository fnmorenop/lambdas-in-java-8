package ocp8_functional_programming.p171_04_functional_programming.p205_working_with_primitives;

import java.util.function.*;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class P208_FunctionMappingTypes {

    public static void main(String[] args) {

        ToDoubleFunction toDoubleFunction = value -> value instanceof String ? Double.valueOf(value.toString()) : (double) value;

        ToIntFunction toIntFunction = value -> Integer.valueOf(value.toString());

        ToLongFunction toLongFunction = value -> Long.valueOf(value.toString());

        Function toStringFunction = value -> value.toString();

        DoubleFunction doubleFunction = value -> String.format("%,.0f", value);

        DoubleUnaryOperator doubleUnaryOperator = Math::sqrt;

        DoubleToIntFunction doubleToIntFunction = value -> (int) (value * value);

        DoubleToLongFunction doubleToLongFunction = value -> (long) (value * 2);

        IntFunction intFunction = String::valueOf;

        IntToDoubleFunction intToDoubleFunction = value -> (double) value;

        IntUnaryOperator intUnaryOperator = operand -> operand % 2;

        IntToLongFunction intToLongFunction = value -> (long) (Math.pow(value, 10));

        LongFunction longFunction = value -> String.valueOf(Math.pow(value, 1/10));

        LongToDoubleFunction longToDoubleFunction = value -> Math.pow(value, 2);

        LongToIntFunction longToIntFunction = value -> (int) value;

        LongUnaryOperator longUnaryOperator = value -> value * 10;

        "The String".
                chars().
                peek(e -> System.out.printf("%n:%d:", e)).
                mapToDouble(intToDoubleFunction).
                peek(e -> System.out.printf(":%f:", e)).
                map(doubleUnaryOperator).
                peek(e -> System.out.printf(":%f:", e)).
                mapToLong(doubleToLongFunction).
                peek(e -> System.out.printf(":%d:", e)).
                mapToInt(longToIntFunction).
                peek(e -> System.out.printf(":%d:", e)).
                map(intUnaryOperator).
                peek(e -> System.out.printf(":%d:", e)).
                mapToLong(intToLongFunction).
                peek(e -> System.out.printf(":%d:", e)).
                map(longUnaryOperator).
                peek(e -> System.out.printf(":%d:", e)).
                mapToDouble(longToDoubleFunction).
                peek(e -> System.out.printf(":%f:", e)).
                mapToObj(doubleFunction).
                peek(e -> System.out.printf(":%s:", e.toString())).
                mapToLong(toLongFunction).
                peek(e -> System.out.printf(":%d:", e)).
                mapToObj(longFunction).
                peek(e -> System.out.printf(":%s:", e)).
                mapToDouble(toDoubleFunction).
                peek(e -> System.out.printf(":%f:", e)).
                mapToInt(doubleToIntFunction).
                peek(e -> System.out.printf(":%d:", e)).
                mapToObj(intFunction).
                peek(e -> System.out.printf(":%s:", e)).
                mapToInt(toIntFunction).
                peek(e -> System.out.printf(":%d:", e)).
                mapToObj(intFunction).
                map(toStringFunction).
                forEach(e -> System.out.printf(":%s:%n", e));
    }
}
