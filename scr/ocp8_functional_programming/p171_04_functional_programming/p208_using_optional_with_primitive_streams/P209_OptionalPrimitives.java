package ocp8_functional_programming.p171_04_functional_programming.p208_using_optional_with_primitive_streams;

import java.util.Optional;
import java.util.*;//OptionalDouble;
import java.util.function.*;//DoubleSupplier;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class P209_OptionalPrimitives {

    public static void main(String[] args) {

        Supplier<IntStream> intStream = () -> IntStream.rangeClosed(0, 10);
        OptionalDouble optionalDouble = intStream.get().average();
        optionalDouble.ifPresent(d -> System.out.printf("%nThe average is: %,.2f%n", d));

        System.out.printf("optionalDouble.isPresent() ? optionalDouble.getAsDouble() : Double.NaN: %f%n",
                optionalDouble.isPresent() ? optionalDouble.getAsDouble() : Double.NaN);

        System.out.printf("optionalDouble.orElseGet(() -> Double.NaN): %f%n",
                optionalDouble.orElseGet(() -> Double.NaN));

        Supplier<IntStream> intStreamSupplier = () -> IntStream.rangeClosed(0, 100);
        //Supplier<IntStream> intStreamSupplier = () -> IntStream.empty();
        Supplier<DoubleStream> doubleStreamSupplier = () -> intStreamSupplier.
                get().
                mapToDouble(i -> (double) i).
                peek(e -> System.out.printf("%f", e));
        Supplier<LongStream> longStreamSupplier = () -> intStreamSupplier.
                get().
                mapToLong(i -> (long) i);
        IntSupplier intSupplier = () -> Integer.MIN_VALUE;
        DoubleSupplier doubleSupplier = () -> Double.NaN;
        LongSupplier longSupplier = () -> Long.MIN_VALUE;

        OptionalInt optionalInt = intStreamSupplier.
                get().
                reduce(Integer::sum);//(acc, e) -> acc + e);
        System.out.printf("%noptionalInt.orElseGet(intSupplier): %d%n",
                optionalInt.orElseGet(intSupplier));

        OptionalDouble optionalDouble1 = doubleStreamSupplier.
                get().//parallel().
                findAny();
        System.out.printf("%noptionalDouble1.orElseGet(doubleSupplier): %.2f%n",
                optionalDouble1.orElseGet(doubleSupplier));

        long longSum = longStreamSupplier.
                get().
                sum();
        System.out.printf("%nlongSum: %d%n",
                longSum);
    }
}
