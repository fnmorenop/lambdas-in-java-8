package ocp8_functional_programming.p171_04_functional_programming.p210_learning_the_functional_interfaces_for_primitives;

import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class P211_FunctionalInterfacesForBoolean {

    public static void main(String[] args) {

        BooleanSupplier booleanSupplier = () -> true;
        BooleanSupplier booleanSupplier1 = () -> Math.random() > .5;

        System.out.printf("%nbooleanSupplier: %b%n", booleanSupplier.getAsBoolean());
        System.out.printf("%nbooleanSupplier1: %b%n", booleanSupplier1.getAsBoolean());

        int i = 0;



        IntStream.
                rangeClosed(0, 100).parallel().
                filter(in -> booleanSupplier1.getAsBoolean()).
                forEach(e -> System.out.printf(":%d:", e));

    }
}
