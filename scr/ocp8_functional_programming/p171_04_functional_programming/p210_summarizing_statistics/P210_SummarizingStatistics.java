package ocp8_functional_programming.p171_04_functional_programming.p210_summarizing_statistics;

import java.io.IOException;
import java.util.IntSummaryStatistics;
import java.util.OptionalInt;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class P210_SummarizingStatistics {

    public static void main(String[] args) {

        Supplier<IntStream> intSupplier =
                //        () -> IntStream.rangeClosed(1, 100);
                        () -> IntStream.empty();

        {

            System.out.println("Thowing exceptions");

            int max = 0;
            try {
                max = max(intSupplier.get());
            } catch (NullPointerException e) {
                System.err.printf("%nWhat a sad exception%n");
                //throw e;
            }

            System.out.printf("%nmax: %d%n", max);
        }

        {
            System.out.println("Summary Statistics");

            int range = 0;
            try {
                range = range (intSupplier.get());
            } catch (NullPointerException e) {
                System.err.printf("%nWhat a sad exception%n");
                //throw e;
            }

            assert (range != 0) : range;

            System.out.printf("%nrange: %d%n", range);
        }
    }

    private static int range(IntStream intStream) {

        IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();
        System.out.printf("%nintSummaryStatistics.getAverage(): %f%n",
                intSummaryStatistics.getAverage());
        System.out.printf("intSummaryStatistics.getCount(): %d%n",
                intSummaryStatistics.getCount());
        if (intSummaryStatistics.getCount() == 0)
            throw new NullPointerException("Very sad exception");

        return intSummaryStatistics.getMax() - intSummaryStatistics.getMin();
    }

    private static int max(IntStream intStream) {

        OptionalInt optionalInt = intStream.max();
        return optionalInt.orElseThrow(() ->
                new NullPointerException("The stream's empty"));
    }
}
