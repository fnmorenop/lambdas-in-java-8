package ocp8_functional_programming.p171_04_functional_programming.p211_functional_interfaces_for_double_int_and_long;

import java.util.function.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.DoubleStream;

public class P211_FunctionalInterfacesForPrimitives {

    public static void main(String[] args) {

        DoubleSupplier doubleSupplier = Math::random;
        IntSupplier intSupplier = () -> (int) (100 * doubleSupplier.getAsDouble());
        LongSupplier longSupplier = () -> (long) (100 * Math.random()) * intSupplier.getAsInt() * 100;

        DoubleConsumer doubleConsumer = d -> System.out.printf(":%.4f:", d);
        IntConsumer intConsumer = i -> System.out.printf(":%d:", i);
        LongConsumer longConsumer = l -> System.out.printf(":%,d:", l);

        System.out.printf("%n%nConsuming suppiers%n%n");

        doubleConsumer.accept(doubleSupplier.getAsDouble());
        intConsumer.accept(intSupplier.getAsInt());
        longConsumer.accept(longSupplier.getAsLong());

        DoublePredicate doublePredicate = d -> d < 0.3D;
        IntPredicate intPredicate = i -> i > 25;
        LongPredicate longPredicate = l -> l > 10_000L && l < 1_000_000L;

        System.out.printf("%n%nUsing Double primitives with predicates%n%n");

        int limit = 100;

        Supplier<DoubleStream> doubleStreamSupplier = () -> DoubleStream.
                generate(doubleSupplier).
                filter(doublePredicate).
                distinct().
                limit(limit).
                sorted();
        doubleStreamSupplier.
                get().
                forEach(doubleConsumer);

        System.out.printf("%n%nUsing Int primitives with predicates%n%n");

        Supplier<IntStream> intStreamSupplier = () -> IntStream.
                generate(intSupplier).
                filter(intPredicate).
                limit(limit).
                sorted();
        intStreamSupplier.
                get().
                forEach(intConsumer);

        System.out.printf("%n%nUsing Long primitives with predicates%n%n");

        Supplier<LongStream> longStreamSupplier = () -> LongStream.
                generate(longSupplier).
                filter(longPredicate).
                limit(limit).
                sorted();
        longStreamSupplier.
                get().
                forEach(longConsumer);

        DoubleFunction<String> doubleFunction = d -> String.format(": %.2f ", d);
        IntFunction<String> intFunction = i -> String.format(": %d ", i);
        LongFunction<String> longFunction = l -> String.format(": %,d ", l *100);

        System.out.printf("%n%nUsing double primitives to Functions%n%n");

        doubleStreamSupplier.
                get().
                mapToObj(doubleFunction).
                forEach(System.out::print);

        System.out.printf("%n%nUsing int primitives to Functions%n%n");

        intStreamSupplier.
                get().
                mapToObj(intFunction).
                forEach(System.out::print);

        System.out.printf("%n%nUsing long primitives to Functions%n%n");

        longStreamSupplier.
                get().
                mapToObj(longFunction).
                forEach(System.out::print);

        DoubleUnaryOperator doubleUnaryOperator = Math::sqrt;
        IntUnaryOperator intUnaryOperator = i -> (int) Math.pow(i, 2);
        LongUnaryOperator longUnaryOperator = l -> (long) Math.log(l);

        System.out.printf("%n%nUsing double unary operator%n%n");

        doubleStreamSupplier.
                get().
                map(doubleUnaryOperator).
                mapToObj(doubleFunction).
                forEach(System.out::print);

        System.out.printf("%n%nUsing int unary operator%n%n");

        intStreamSupplier.
                get().
                map(intUnaryOperator).
                mapToObj(intFunction).
                forEach(System.out::print);

        System.out.printf("%n%nUsing long unary operator%n%n");

        longStreamSupplier.
                get().
                map(longUnaryOperator).
                mapToObj(longFunction).
                forEach(System.out::print);

        DoubleBinaryOperator doubleBinaryOperator =
                (d1, d2) -> Math.pow(d1, 2) + Math.pow(d2, 2);
        IntBinaryOperator intBinaryOperator =
                (i1, i2) -> (int) Math.pow(i1, i2);
        LongBinaryOperator longBinaryOperator =
                (l1, l2) -> (long) (Math.sqrt(l1) + Math.sqrt(l2));

        System.out.printf("%n%nUsing double binary operator%n%n");

        doubleStreamSupplier.
                get().
                reduce(doubleBinaryOperator).
                ifPresent(doubleConsumer);

        System.out.printf("%n%nUsing int binary operator%n%n");

        intStreamSupplier.
                get().
                reduce(intBinaryOperator).
                ifPresent(intConsumer);

        System.out.printf("%n%nUsing long binary operator%n%n");

        longStreamSupplier.
                get().
                reduce(longBinaryOperator).
                ifPresent(longConsumer);


    }
}
