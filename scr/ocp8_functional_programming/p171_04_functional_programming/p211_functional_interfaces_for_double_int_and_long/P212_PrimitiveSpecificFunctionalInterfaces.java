package ocp8_functional_programming.p171_04_functional_programming.p211_functional_interfaces_for_double_int_and_long;

import java.util.Random;
import java.util.function.*;
import java.util.regex.Matcher;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class P212_PrimitiveSpecificFunctionalInterfaces {

    public static void main(String[] args) {

        int limit = 100;

        DoubleConsumer doubleConsumer = d -> System.out.printf(": %.1f ", d);
        IntConsumer intConsumer = i -> System.out.printf(": %,d ", i);
        LongConsumer longConsumer = l -> System.out.printf(": %,d ", l);
        Consumer<String> stringConsumer = s -> System.out.printf(": %s ", s);

        Supplier<DoubleStream> doubleSupplier =
                () -> DoubleStream.
                        iterate(0.1D, d -> d + 0.1D).
                        limit(limit);

        System.out.printf("%n%ndoubleSupplier%n%n");

        doubleSupplier.
                get().
                forEach(doubleConsumer);

        Supplier<IntStream> intSupplier =
                () -> IntStream.
                        iterate(1, i -> i + 1).
                        limit(limit);

        System.out.printf("%n%nintSupplier%n%n");

        intSupplier.
                get().
                forEach(intConsumer);

        Supplier<LongStream> longSupplier =
                () -> LongStream.
                        iterate(1000000, i -> i + 1000000).
                        limit(limit);

        System.out.printf("%n%nlongSupplier%n%n");

        longSupplier.
                get().
                forEach(longConsumer);

        Supplier<Stream<String>> stringSupplier =
                () -> Stream.
                        generate(
                                () -> new Random().
                                        ints(' ', 'z').
                                        parallel().
                                        filter(e -> Character.toString((char) e).matches("[a-zA-Z ]")).
                                        limit((long) (Math.random() * 20)).
                                        mapToObj(c -> Character.toString((char) c)).
                                        reduce(String::concat).
                                        orElse("_")
                        ).
                        limit(limit);

        System.out.printf("%n%nStringSupplier%n%n");

        stringSupplier.
                get().
                forEach(stringConsumer);

        ToDoubleFunction<String> toDoubleFunction =
                s -> Math.pow(s.length(), 2);
        ToIntFunction<String> toIntFunction =
                s -> s.length() * 2 + 15;
        ToLongFunction<String> toLongFunction =
                s -> (long) (Math.random() * 100_000_000) * s.length();

        System.out.printf("%n%ntoDoubleFunction%n%n");

        stringSupplier.
                get().
                sequential().
                peek(stringConsumer).
                mapToDouble(toDoubleFunction).
                forEach(doubleConsumer);

        System.out.printf("%n%ntoIntFunction%n%n");

        stringSupplier.
                get().
                sequential().
                peek(stringConsumer).
                mapToInt(toIntFunction).
                forEach(intConsumer);

        System.out.printf("%n%ntoLongFunction%n%n");

        stringSupplier.
                get().
                sequential().
                peek(stringConsumer).
                mapToLong(toLongFunction).
                forEach(longConsumer);

        ToDoubleBiFunction<String, String> toDoubleBiFunction =
                (s1, s2) -> (double) Math.pow(s1.concat(s2).length(), 2);
        ToIntBiFunction<String, Integer> toIntBiFunction =
                (s, i) -> (int) (Math.sqrt(s.length()) * i);
        ToLongBiFunction<Stream<String>, Stream<String>> toLongBiFunction =
                (sta, st) -> Stream.
                        concat(sta, st).

                        collect(
                                () -> LongStream.builder(),
                                (LongStream.Builder sts, String s) ->
                                        sts.accept(toLongFunction.applyAsLong(s)),
                                (sa, sb) -> LongStream.concat(sa.build(), sb.build())
                        ).
                        build().

                        /*
                        flatMapToLong(
                                s -> Stream.
                                        of(s).
                                        mapToLong(toLongFunction)
                        ).
                        */
                        peek(longConsumer).
                        reduce(Long::sum).
                        orElse(0L);

        System.out.printf("%n%nToDoubleBiFunction%n%n");

        long tolongf = toLongBiFunction.applyAsLong(stringSupplier.get(), stringSupplier.get());
        System.out.println("\ntoLong: ");
        longConsumer.accept(tolongf);

        DoubleStream.Builder doubleStream = DoubleStream.builder();
        ObjDoubleConsumer<DoubleStream> objDoubleConsumer =
                (st, d) -> st.forEach(dst -> doubleStream.accept(dst * d));

        System.out.printf("%n%nObjDoubleConsumer%n%n");

        double seedObjDoubleConsumer = Math.random() * 100;

        objDoubleConsumer.accept(doubleSupplier.get().peek(doubleConsumer), seedObjDoubleConsumer);

        System.out.printf("%n%nDoubleStream.Builder%n%n");

        doubleStream.build().forEach(doubleConsumer);


    }
}
