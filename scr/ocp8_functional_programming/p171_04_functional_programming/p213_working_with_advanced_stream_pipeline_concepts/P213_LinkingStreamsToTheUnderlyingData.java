package ocp8_functional_programming.p171_04_functional_programming.p213_working_with_advanced_stream_pipeline_concepts;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class P213_LinkingStreamsToTheUnderlyingData {

    public static void main(String[] args) {

        {
            List<String> cats = new ArrayList<>();
            cats.add("Annie");
            cats.add("Ripley");
            Stream stream = cats.stream();
            cats.add("KC");
            cats.add("Van");

            System.out.printf("%n%n stream: %d%n%n", stream.
                    peek(Printer.stringConsumer).
                    count());
        }
    }
}
