package ocp8_functional_programming.p171_04_functional_programming.p213_working_with_advanced_stream_pipeline_concepts;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.util.Optional;
import java.util.stream.DoubleStream;

public class P214_ChainingOptionals {

    public static void main(String[] args) {

        Optional<Integer> optional = Optional.of(123);
        Printer.title.accept("optional");
        Printer.intConsumer.accept(optional.get());

        oldThreeDigit(optional);
        threeDigit(optional);

        Printer.title.accept ("ThreeDigit string");
        Optional<String> stringOptional = Optional.of("123456");
        getLenght(stringOptional).
                ifPresent(Printer.intConsumer::accept);
        getLenght(stringOptional).
                ifPresent(Printer.consumer);

        Printer.title.accept ("Chaining optional calculator");
        Optional<Integer> result = stringOptional.
                flatMap(P214_ChainingOptionals::calculator);
        result.ifPresent(Printer.intConsumer::accept);
        result.ifPresent(Printer.consumer);
        Printer.optionalConsumer.accept(result);
        Printer.consumer.accept(result);


        Printer.title.accept ("Consuming Streams");
        Printer.consumer.accept(DoubleStream.iterate(1, n -> n + 1).limit(100));
    }

    private static void oldThreeDigit (Optional<Integer> optional) {

        Printer.title.accept("OldThreeDigit string");

        if ( optional.isPresent() ) {
            Integer num = optional.get();
            String string = "" + num;
            if ( string.length() == 3 )
                Printer.stringConsumer.accept(string);
        }
    }

    private static void threeDigit ( Optional<Integer> optional ) {

        Printer.title.accept ("ThreeDigit string");
        optional.
                map ( i -> "" + i ).
                filter ( s -> s.length() == 3 ).
                ifPresent ( Printer.stringConsumer );
    }

    private static Optional<Integer> getLenght (Optional<String> optional) {

        return optional.map(String::length);
    }

    private static Optional<Integer> calculator ( String s ) {

        return Optional.of(s).map(String::length);
    }
}
