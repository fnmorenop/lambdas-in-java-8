package ocp8_functional_programming.p171_04_functional_programming.p213_working_with_advanced_stream_pipeline_concepts;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.io.IOException;
import java.util.List;
import java.util.function.Supplier;

public class P215_CheckedExceptinsAndFunctionalInterfaces {

    public static void main(String[] args) {

        // create().stream().count(); // Doesn't compile
        try {
            create().stream().count();  // Throws exception
        } catch (IOException e) {
            e.printStackTrace();
        }

        Printer.title.accept("s2");

        // Supplier<List<String>> s = P215_CheckedExceptinsAndFunctionalInterfaces::create; // Doesn't compile
        Supplier<List<String>> s =
                () -> {
                    try {
                        return create();
                    } catch (IOException e) {
                        throw new RuntimeException("\n\nError ----> RuntimeException\n\n");
                    }
                };
        //s.get(); // Throws exception
        Printer.title.accept("s2");

        Supplier<List<String>> s2 = P215_CheckedExceptinsAndFunctionalInterfaces::createSafe;
        s2.get(); // Throws exception
    }

    private static List<String> create () throws IOException {
        throw new IOException("\n\nError --->\n\n");
    }

    private static List<String> createSafe () {
        try {
            return create();
        } catch (IOException e) {
            throw new RuntimeException("\n\nError ----> RuntimeException ----> Safe\n\n");
        }
    }
}
