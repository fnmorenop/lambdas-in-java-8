package ocp8_functional_programming.p171_04_functional_programming.p217_collecting_results;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class P218_CollectingUsingBasicCollectors {

    public static String baseDir = "C:\\learn\\lambdas-in-java-8\\scr\\ocp8_functional_programming\\p171_04_functional_programming\\p217_collecting_results";

    public static void main(String[] args) {

        Printer.title.accept("Animals count");
        Printer.consumer.accept(getStringStream().get().count());
        getStringStream().
                get().
                forEach(Printer.consumer);

        Printer.title.accept("Collect joining charset");
        Printer.consumer.accept(
                getStringStream().
                        get().
                        //collect(Collectors.joining(" | =) | ", "<The Animals> ", " </The Animals>"))
                        collect(
                                Collectors.
                                        joining(
                                                Stream.
                                                        iterate(
                                                                new StringBuilder("-"),
                                                                sb -> sb.append("-")
                                                        ).
                                                        //peek(Printer.consumer::accept).
                                                        limit(3).
                                                        collect(Collectors.joining()),
                                        "<The Animals> ",
                                        " </The Animals>"
                                        )
                        )
        );

        ListIterator it = IntStream.rangeClosed(0, (int) getStringStream().get().count()).boxed().collect(Collectors.toList()).listIterator();
        ListIterator it2 = IntStream.rangeClosed(1, (int) getStringStream().get().count()).boxed().collect(Collectors.toList()).listIterator();



        Printer.title.accept("Average int");
        Printer.consumer.accept(
                getStringStream().
                        get().
                        //filter(s -> Math.random() < 0.02D).
                        filter(s -> {
                            if (it.nextIndex() % 2 == 0 || it.nextIndex() % 3 == 0) {
                                it.next();
                                return true;
                            } else {
                                it.next();
                                return false;
                            }
                        }).
                        //peek(Printer.consumer).
                        map(s -> it2.next() + " " + s).
                        //limit(10).
                        peek(Printer.consumer).
                        collect(Collectors.averagingInt(String::length))
        );

        Printer.title.accept("Collect to Collection");
        Printer.consumer.accept(
                getStringStream().
                        get().
                        filter((s) -> ((String)s).startsWith("T")).
                        //peek(Printer.consumer).
                        collect(Collectors.toCollection(TreeSet::new))
        );

        Printer.title.accept("Collect to Set");
        Printer.consumer.accept(
                getStringStream().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        //peek(Printer.consumer).
                        filter((s) -> ((String)s).startsWith("T")).
                        //peek(Printer.consumer).
                        collect(Collectors.toSet())
        );

        System.out.println(Stream.iterate(1, x -> ++x).limit(5).map(x -> "" + x).collect(Collectors.
                joining()));


    }

    public static Supplier<Stream> getStringStream () {

        String animalsFile = "animals.txt";

        return () -> {
            try {
                return Files.
                        readAllLines(Paths.get(baseDir, animalsFile), Charset.forName("utf-8")).
                        stream();
            } catch (IOException e) {
                return Stream.of(e.toString());
            }
        };
    }
}
