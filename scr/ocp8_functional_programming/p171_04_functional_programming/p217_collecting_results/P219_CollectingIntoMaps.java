package ocp8_functional_programming.p171_04_functional_programming.p217_collecting_results;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Random;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class P219_CollectingIntoMaps {

    public static void main(String[] args) {

        Supplier<Stream> animals = P218_CollectingUsingBasicCollectors.getStringStream();

        Printer.title.accept("animals");
        Printer.consumer.accept(animals);

        Printer.title.accept("Animals to map, mapping length for value");
        Printer.consumer.accept(
                animals.
                        get().
                        collect(
                                Collectors.
                                        toMap(
                                                Function.identity(),
                                                String::length
                                        )
                        )
        );

        Printer.title.accept("Animals to map, mapping random iterator of int as key");

        Supplier<ListIterator<Integer>> supIt = () -> new Random().
                ints((int) animals.get().count(),
                        0,
                        (int) animals.get().count()).
                boxed().
                //sequential().
                //sorted().
                collect(Collectors.toList()).
                listIterator();

        ListIterator<Integer> intIt = supIt.get();

        Printer.consumer.accept(
                animals.
                        get().
                        collect(
                                Collectors.
                                        toMap(
                                                k -> Integer.valueOf(
                                                        String.format(
                                                                "%d",
                                                                intIt.nextIndex()
                                                        )),
                                                v -> //Function.identity()
                                                        String.format(
                                                                "%s:%d",
                                                                v,
                                                                intIt.next()
                                                        )
                                        )
                        )//.getClass()
        );

        Printer.title.accept("Animals to map, mapping length for key :: HashMap");
        Printer.consumer.accept(
                animals.
                        get().
                        filter(e -> Math.random() < 0.2D).
                        //collect(Collectors.toMap(String::length, Function.identity())) // IllegalStateException: Duplicate key
                        collect(Collectors.toMap(
                                String::length,
                                Function.identity(),
                                (s1, s2) -> s1 + "| " + s2)
                        )//.getClass()
        );

        Printer.title.accept("Animals to map, mapping length for key :: TreeMap");
        Printer.consumer.accept(
                animals.
                        get().
                        filter(e -> Math.random() < 0.2D).
                        //collect(Collectors.toMap(String::length, Function.identity())) // IllegalStateException: Duplicate key
                                collect(Collectors.toMap(
                                String::length,
                                Function.identity(),
                                (s1, s2) -> s1 + ", " + s2,
                                TreeMap::new)
                        )//.getClass()
        );

        Printer.title.accept("Animals source");
        Iterator<String> animalsSource =
                animals.
                        get().
                        filter(s -> Math.random() < 0.2D).
                        peek(Printer.consumer).
                        iterator();
        //Printer.consumer.accept(animalsSource);

        Printer.title.accept("Animals to map, mapping value to list and merging another stream by iterator:: TreeMap");
        Printer.consumer.accept(
                animals.
                        get().
                        filter(e -> Math.random() < 0.2D).
                        //collect(Collectors.toMap(String::length, Function.identity())) // IllegalStateException: Duplicate key
                                collect(Collectors.toMap(
                                        String::length,
                                        s -> Stream.of(
                                                animalsSource.hasNext() ?
                                                        "<" + s + " - " + animalsSource.next() + ">" :
                                                        "<" + s + ">"
                                                ).
                                                collect(Collectors.toList()),
                                        (l, e) -> Stream.concat(l.stream(), e.stream()).
                                                collect(Collectors.toList()
                                                ),
                                        TreeMap::new
                                )
                        )//.getClass()
        );
    }
}
