package ocp8_functional_programming.p171_04_functional_programming.p217_collecting_results;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;

import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P220_CollectionUsingGroupingPartitioningAndMapping {

    static Supplier<Stream> animals = P218_CollectingUsingBasicCollectors.getStringStream();

    public static void main(String[] args) {

        Printer.title.accept("groupingBy length");
        Printer.consumer.accept(
                animals.
                        get().
                        filter(e -> Math.random() < 0.2D).
                        collect(Collectors.groupingBy(String::length))
        );


    }
}
