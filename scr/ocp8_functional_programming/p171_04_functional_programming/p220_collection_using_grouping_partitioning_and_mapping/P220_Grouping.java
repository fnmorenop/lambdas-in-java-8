package ocp8_functional_programming.p171_04_functional_programming.p220_collection_using_grouping_partitioning_and_mapping;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;
import ocp8_functional_programming.p000_00_selfstudy.util.Streams;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class P220_Grouping {

    public static void main(String[] args) {

        Printer.title.accept("Grouping");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(String::length)
                        )
        );

        Printer.title.accept("Grouping by chartAt(0)");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(s -> s.charAt(0))
                        )
        );

        Printer.title.accept("Grouping by subString");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(s -> s.substring(0, 3))
                        )
        );

        Printer.title.accept("Grouping by matching");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(s -> s.matches("^[A-O]+[aeo]*.*$"))
                        )
        );

        Printer.title.accept("Grouping by Random");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(s -> (int) (20 * Math.random()))
                        )
        );

        Printer.title.accept("Grouping Collection by set");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(
                                                s -> s.charAt(0),
                                                Collectors.toSet()
                                        )
                        )
        );

        Printer.title.accept("Grouping by grouping");
        Printer.consumer.accept(
                Streams.
                        getAnimals().
                        get().
                        filter(s -> Math.random() < 0.2D).
                        collect(
                                Collectors.
                                        groupingBy(
                                                s -> s.charAt(0),
                                                Collectors.groupingBy(
                                                        s -> s.substring(0, 3)
                                                )
                                        )
                        )
        );

        Printer.title.accept("Grouping Collection by set");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                Collectors.
                                        groupingBy(
                                                Function.identity(),
                                                TreeMap::new,
                                                Collectors.toSet()
                                        )
                        )
        );

        Printer.title.accept("Grouping Collection by list");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                Collectors.
                                        groupingBy(
                                                Function.identity(),
                                                TreeMap::new,
                                                Collectors.toList()
                                        )
                        )
        );

        Printer.title.accept("Grouping ConcurrentHashMap Collection by list");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                Collectors.
                                        groupingByConcurrent(
                                                Function.identity(),
                                                ConcurrentHashMap::new,
                                                Collectors.toList()
                                        )
                        )
        );

        Printer.title.accept("Grouping by counting");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                Collectors.
                                        groupingByConcurrent(
                                                Function.identity(),
                                                ConcurrentHashMap::new,
                                                Collectors.counting()
                                        )
                        )
        );
    }
}
