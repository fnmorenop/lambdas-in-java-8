package ocp8_functional_programming.p171_04_functional_programming.p220_collection_using_grouping_partitioning_and_mapping;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;
import ocp8_functional_programming.p000_00_selfstudy.util.Streams;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class P221_Partitioning {

    public static void main(String[] args) {

        Printer.title.accept("Partitioning");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 15
                                        )
                        )
        );

        Printer.title.accept("Partitioning to set");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 15,
                                                Collectors.toSet()
                                        )
                        )
        );

        Printer.title.accept("Partitioning to list");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 15,
                                                Collectors.toList()
                                        )
                        )
        );

        Printer.title.accept("Partitioning to set recurrent");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 5,
                                                partitioningBy(
                                                        i -> i <= 5,
                                                        Collectors.toSet()
                                                )
                                        )
                        )
        );

        Printer.title.accept("Partitioning to set recurrent by counting");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 5,
                                                partitioningBy(
                                                        i -> i <= 5,
                                                        Collectors.groupingBy(
                                                                Function.identity(),
                                                                Collectors.counting()
                                                        )
                                                )
                                        )
                        )
        );

        Printer.title.accept("Partitioning to set recurrent by counting to list");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i >= 5,
                                                partitioningBy(
                                                        i -> i <= 5,
                                                        Collectors.groupingBy(
                                                                Function.identity(),
                                                                Collectors.toList()
                                                        )
                                                )
                                        )
                        )
        );

        Printer.title.accept("Partitioning mapping");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                        i -> i % 2 == 1,
                                                mapping(
                                                        i -> {
                                                            if (i % 2 == 1)
                                                                return i * 3;
                                                            else
                                                                return i * 2;},
                                                        minBy(
                                                                Comparator.comparingInt(i -> i)
                                                        )
                                                )
                                        )
                        )
        );

        Printer.title.accept("Partitioning mapping using static Collectors methods");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                                i -> i % 2 == 1,
                                                mapping(
                                                        i -> {
                                                            if (i % 2 == 1)
                                                                return i * 3;
                                                            else
                                                                return i * 2;}/*,
                                                        Collectors.minBy(
                                                                Comparator.comparingInt(i -> i)
                                                        )*/, toCollection(LinkedHashSet::new)
                                                )
                                        )
                        )
        );
    }
}
