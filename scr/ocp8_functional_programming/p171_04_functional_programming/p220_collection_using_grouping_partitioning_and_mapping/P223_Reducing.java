package ocp8_functional_programming.p171_04_functional_programming.p220_collection_using_grouping_partitioning_and_mapping;

import ocp8_functional_programming.p000_00_selfstudy.util.Printer;
import ocp8_functional_programming.p000_00_selfstudy.util.Streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

import static java.util.stream.Collectors.*;

public class P223_Reducing {

    public static void main(String[] args) {

        Printer.title.accept("Reducing");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                        i -> i % 2 == 1,
                                        mapping(
                                                i -> {
                                                    if (i % 2 == 1)
                                                        return i * 3;
                                                    else
                                                        return i * 2;},
                                                reducing(Integer::sum)
                                        )
                                )
                        )
        );

        Printer.title.accept("Reducing to list");
        Printer.consumer.accept(
                Streams.
                        getDuplicatedNumbers(10).
                        get().
                        boxed().
                        collect(
                                partitioningBy(
                                        i -> i % 2 == 1,
                                        mapping(
                                                i -> {
                                                    if (i % 2 == 1)
                                                        return i * 3;
                                                    else
                                                        return i * 2;},
                                                reducing(
                                                        "",
                                                        (s, i) -> s.toString().concat(", ").concat(i.toString())
                                                )
                                        )
                                )
                        )
        );


    }
}
